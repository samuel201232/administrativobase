
function ValidaNumero(event) {
  if (event.keyCode == 46) //Bizarramente, no Chrome o ponto final é identificado como KeyCode 46
        return false;
        if (event.keyCode == 8 || event.keyCode == 46
         || event.keyCode == 37 || event.keyCode == 39) {
            return true;
        }
        else if (event.keyCode < 48 || event.keyCode > 57) {
            return false
        }
        else return true;
}


function FormataCelular(element, e){
        //ESSA FUNÇÃO É PARA O OnKeyUp
          var length = element.value.length;
           if(e.keyCode != 8){
            if (length == 2){
              if (element.value.charAt(0)!="(")
                element.value = "(" + element.value + ") ";
            }
            if (length == 3)
              if (element.value.charAt(0)=="(")
                element.value += ") ";
            if (length == 10){
              element.value += "-";
            }
            if (length == 11)
                RetornaChar(element, "-", 10);
            if (length == 15)
                telefone = element.value.substring(0, 15);
            if (length > 15){
              element.value = telefone;
            }
           }else{
            if (length == 4)
                 element.value = "";                          
           }     
}
                  
function FormataTelefone(element, e){
       //ESSA FUNÇÃO É PARA O OnKeyUp
       length = element.value.length;
       if(e.keyCode != 8){
        if (length == 2){
          if (element.value.charAt(0)!="(")
            element.value = "(" + element.value + ") ";
        }
        if (length == 3)
          if (element.value.charAt(0)=="(")
            element.value += ") ";
        if (length == 9){
          element.value += "-";
        }
        if (length == 10)
                RetornaChar(element, "-", 9);
        if (length == 14)
            telefone = element.value.substring(0, 14);
        if (length > 14){
          element.value = telefone;                  
        }

       }    
}

function FormataCep(element, e){
     //ESSA FUNÇÃO É PARA O OnKeyUp
     var length = element.value.length;
       if(e.keyCode != 8){
        if (length == 5)
          element.value += "-";
        if (length == 6)
            RetornaChar(element, "-", 5);
        if (length == 9){
          cep = element.value.substring(0, 9);
        }  if (length > 9){
          element.value = cep;
       }
    }
}

function FormataCelParaTel(element){ 
        //ESSA FUNÇÃO É PARA O OnBlur
        var length = element.value.length;
        //alert(length);
        if (length == 14){

            element.value = element.value.replace(/\-/g,"");
            element.value = element.value.substring(0,9) + "-" +element.value.substring(9,14);

        }
        SaidaCampo(element, 14);

}

function FormataNascimento(element, e){ //Formata datas
     //ESSA FUNÇÃO É PARA O OnKeyUp
     var length = element.value.length;
       if(e.keyCode != 8){
        if (length == 2)
          element.value += "/";
        if (length == 3)
          RetornaChar(element, "/", 2);
        if (length == 5)
          element.value += "/";
        if (length == 6)
          RetornaChar(element, "/", 5);
        if (length == 10){
          nascimento = element.value.substring(0, 10);
        }  if (length > 10){
          element.value = nascimento;
       }
    }
}
/**
 * DHTML date validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/datevalidation.asp)
 */
// Declaring valid date character, minimum year and maximum year


function isInteger(s){
	var i;
    for (i = 0; i < s.length; i++){   
        // Check that current character is number.
        var c = s.charAt(i);
        if (((c < "0") || (c > "9"))) return false;
    }
    // All characters are numbers.
    return true;
}

function stripCharsInBag(s, bag){
	var i;
    var returnString = "";
    // Search through string's characters one by one.
    // If character is not in bag, append to returnString.
    for (i = 0; i < s.length; i++){   
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1) returnString += c;
    }
    return returnString;
}

function daysInFebruary (year){
	// February has 29 days in any year evenly divisible by four,
    // EXCEPT for centurial years which are not also divisible by 400.
    return (((year % 4 == 0) && ( (!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28 );
}
function DaysArray(n) {
	for (var i = 1; i <= n; i++) {
		this[i] = 31
		if (i==4 || i==6 || i==9 || i==11) {this[i] = 30}
		if (i==2) {this[i] = 29}
   } 
   return this
}

function SaidaNascimento(dtStr){
        var dtCh= "/"
        var minYear=1900
        var maxYear= new Date().getFullYear()
	var daysInMonth = DaysArray(12)
	var pos1=dtStr.value.indexOf(dtCh)
	var pos2=dtStr.value.indexOf(dtCh,pos1+1)
	var strDay=dtStr.value.substring(0,pos1)
	var strMonth=dtStr.value.substring(pos1+1,pos2)
	var strYear=dtStr.value.substring(pos2+1)
	strYr=strYear
	if (strDay.charAt(0)=="0" && strDay.length>1) strDay=strDay.substring(1)
	if (strMonth.charAt(0)=="0" && strMonth.length>1) strMonth=strMonth.substring(1)
        for (var i = 1; i <= 3; i++) {
		if (strYr.charAt(0)=="0" && strYr.length>1) strYr=strYr.substring(1)
	}
	month=parseInt(strMonth)
	day=parseInt(strDay)
	year=parseInt(strYr)
	if (strMonth.length<1 || month<1 || month>12){
		alert("O MÊS: "+strMonth+" \nEsta Incorreto")
                SaidaCampo(dtStr,20)
		//return false
	}
	if (strDay.length<1 || day<1 || day>31 || (month==2 && day>daysInFebruary(year)) || day > daysInMonth[month]){
		alert("O DIA: "+strDay+" \nEsta Incorreto")
                SaidaCampo(dtStr,20)
		//return false
	}
	if (strYear.length != 4 || year==0 || year<minYear || year>maxYear){
		alert("O ano correto deve ser entre "+minYear+" e "+maxYear)
                SaidaCampo(dtStr,20)
		//return false
	}
	if (dtStr.indexOf(dtCh,pos2+1)!=-1 || isInteger(stripCharsInBag(dtStr, dtCh))==false){
		alert("Please enter a valid date")
                SaidaCampo(dtStr,20)
		//return false
	}
        //SaidaCampo(dtStr,10)
return true
}

function SaidaCampo(element,max){ // Função para limpar o campo se não estiver totalmente preenchido
        //ESSA FUNÇÃO É PARA O OnBlur
        var length = element.value.length;
        if(length < max){
           element.value = ""; 
        }
}

function RetornaChar(element,str,num){ // Caso o usuario apague o caractere da mascara, a mesma retorna na posição inicial
    if (element.value.charAt(num)!=str)
                element.value = element.value.substring(0, num) + str + element.value.substring(num, num+1);
}