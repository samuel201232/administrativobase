(function() {
    "use strict";

    // custom scrollbar

   /* $("html").niceScroll({styler:"fb",cursorcolor:"#1ABC9C", cursorwidth: '6', cursorborderradius: '10px', background: '#424f63', spacebarenabled:false, cursorborder: '0',  zindex: '1000'});

    $(".scrollbar1").niceScroll({styler:"fb",cursorcolor:"rgba(97, 100, 193, 0.78)", cursorwidth: '6', cursorborderradius: '0',autohidemode: 'false', background: '#F1F1F1', spacebarenabled:false, cursorborder: '0'});
	
    $(".scrollbar1").getNiceScroll();
    if ($('body').hasClass('scrollbar1-collapsed')) {
        $(".scrollbar1").getNiceScroll().hide();
    }*/

    /**** BLOCO PARA O JAVASCRIPT DO BOLETIM 1 AO 5 ****/

    // Ao digitar nos campos do 1° Bimestre - Calcula a nota do primeiro bimestre e tenta calcular os pontos finais
    $( ".calc_1ao51b_mensal,.calc_1ao51b_bimestral,.calc_1ao51b_nam,.calc_1ao51b_extra,.calc_1ao51b_recuperacao" ).keyup(function( event ) {
        var calc_1ao51b_nota        = $(this).closest('tr').find('.calc_1ao51b_nota');
        var calc_1ao51b_mensal      = parseFloat($(this).closest('tr').find('.calc_1ao51b_mensal').val());
        var calc_1ao51b_bimestral   = parseFloat($(this).closest('tr').find('.calc_1ao51b_bimestral').val());
        var calc_1ao51b_nam         = parseFloat($(this).closest('tr').find('.calc_1ao51b_nam').val());
        var calc_1ao51b_extra       = parseFloat($(this).closest('tr').find('.calc_1ao51b_extra').val());
        var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
        var resultado = calculoMedia1ao5(calc_1ao51b_nam,calc_1ao51b_mensal,calc_1ao51b_bimestral,calc_1ao51b_extra,calc_1ao51b_recuperacao);

        if(resultado){
            calc_1ao51b_nota.val(resultado);
            var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
            var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
            var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
            var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

            var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
            var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
            var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
            var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

            var calc_1ao5exame          = parseFloat($(this).closest('tr').find('.calc_1ao5exame').val());

            var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
            var status      = $(this).closest('tr').find('.calc_1ao5status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao51b_nota.val("");
        }
    });

    // Ao digitar nos campos do 2° Bimestre - Calcula a nota do segundo bimestre e tenta calcular os pontos finais
    $( ".calc_1ao52b_mensal,.calc_1ao52b_bimestral,.calc_1ao52b_nam,.calc_1ao52b_extra,.calc_1ao52b_recuperacao" ).keyup(function( event ) {
        var calc_1ao52b_nota        = $(this).closest('tr').find('.calc_1ao52b_nota');
        var calc_1ao52b_mensal      = parseFloat($(this).closest('tr').find('.calc_1ao52b_mensal').val());
        var calc_1ao52b_bimestral   = parseFloat($(this).closest('tr').find('.calc_1ao52b_bimestral').val());
        var calc_1ao52b_nam         = parseFloat($(this).closest('tr').find('.calc_1ao52b_nam').val());
        var calc_1ao52b_extra       = parseFloat($(this).closest('tr').find('.calc_1ao52b_extra').val());
        var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
        var resultado = calculoMedia1ao5(calc_1ao52b_nam,calc_1ao52b_mensal,calc_1ao52b_bimestral,calc_1ao52b_extra,calc_1ao52b_recuperacao);

        if(resultado){
            calc_1ao52b_nota.val(resultado);
            var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
            var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
            var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
            var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

            var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
            var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
            var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
            var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

            var calc_1ao5exame          = parseFloat($(this).closest('tr').find('.calc_1ao5exame').val());

            var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
            var status      = $(this).closest('tr').find('.calc_1ao5status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao52b_nota.val("");
        }
    });

    // Ao digitar nos campos do 3° Bimestre - Calcula a nota do terceiro bimestre e tenta calcular os pontos finais
    $( ".calc_1ao53b_mensal,.calc_1ao53b_bimestral,.calc_1ao53b_nam,.calc_1ao53b_extra,.calc_1ao53b_recuperacao" ).keyup(function( event ) {
        var calc_1ao53b_nota        = $(this).closest('tr').find('.calc_1ao53b_nota');
        var calc_1ao53b_mensal      = parseFloat($(this).closest('tr').find('.calc_1ao53b_mensal').val());
        var calc_1ao53b_bimestral   = parseFloat($(this).closest('tr').find('.calc_1ao53b_bimestral').val());
        var calc_1ao53b_nam         = parseFloat($(this).closest('tr').find('.calc_1ao53b_nam').val());
        var calc_1ao53b_extra       = parseFloat($(this).closest('tr').find('.calc_1ao53b_extra').val());
        var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
        var resultado = calculoMedia1ao5(calc_1ao53b_nam,calc_1ao53b_mensal,calc_1ao53b_bimestral,calc_1ao53b_extra,calc_1ao53b_recuperacao);

        if(resultado){
            calc_1ao53b_nota.val(resultado);
            var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
            var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
            var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
            var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

            var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
            var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
            var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
            var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

            var calc_1ao5exame          = parseFloat($(this).closest('tr').find('.calc_1ao5exame').val());

            var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
            var status      = $(this).closest('tr').find('.calc_1ao5status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao53b_nota.val("");
        }
    });

    // Ao digitar nos campos do 4° Bimestre - Calcula a nota do quarto bimestre e tenta calcular os pontos finais
    $( ".calc_1ao54b_mensal,.calc_1ao54b_bimestral,.calc_1ao54b_nam,.calc_1ao54b_extra,.calc_1ao54b_recuperacao" ).keyup(function( event ) {
        var calc_1ao54b_nota        = $(this).closest('tr').find('.calc_1ao54b_nota');
        var calc_1ao54b_mensal      = parseFloat($(this).closest('tr').find('.calc_1ao54b_mensal').val());
        var calc_1ao54b_bimestral   = parseFloat($(this).closest('tr').find('.calc_1ao54b_bimestral').val());
        var calc_1ao54b_nam         = parseFloat($(this).closest('tr').find('.calc_1ao54b_nam').val());
        var calc_1ao54b_extra       = parseFloat($(this).closest('tr').find('.calc_1ao54b_extra').val());
        var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());
        var resultado = calculoMedia1ao5(calc_1ao54b_nam,calc_1ao54b_mensal,calc_1ao54b_bimestral,calc_1ao54b_extra,calc_1ao54b_recuperacao);

        if(resultado){
            calc_1ao54b_nota.val(resultado);
            var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
            var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
            var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
            var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

            var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
            var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
            var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
            var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

            var calc_1ao5exame          = parseFloat($(this).closest('tr').find('.calc_1ao5exame').val());

            var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
            var status      = $(this).closest('tr').find('.calc_1ao5status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao54b_nota.val("");
        }
    });

    // Ao digitar nos campos da recuperação (todos os bimestres) - Tenta calcular os pontos finais
    $( ".calc_1ao51b_recuperacao,.calc_1ao52b_recuperacao,.calc_1ao53b_recuperacao,.calc_1ao54b_recuperacao" ).keyup(function( event ) {
        var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
        var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
        var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
        var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

        var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
        var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
        var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
        var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

        var calc_1ao5exame          = parseFloat($(this).closest('tr').find('.calc_1ao5exame').val());

        var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
        var status      = $(this).closest('tr').find('.calc_1ao5status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    // Ao digitar nos campos da exame (todos os bimestres) - Calcula o STATUS
    $( ".calc_1ao5exame" ).keyup(function( event ) {
        var calc_1ao51b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao51b_nota').val());
        var calc_1ao52b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao52b_nota').val());
        var calc_1ao53b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao53b_nota').val());
        var calc_1ao54b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao54b_nota').val());

        var calc_1ao51b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao51b_recuperacao').val());
        var calc_1ao52b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao52b_recuperacao').val());
        var calc_1ao53b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao53b_recuperacao').val());
        var calc_1ao54b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao54b_recuperacao').val());

        var calc_1ao5exame          = parseFloat($(this).val());

        var calc_pontos = calculoPontos(calc_1ao51b_nota,calc_1ao51b_recuperacao,calc_1ao52b_nota,calc_1ao52b_recuperacao,calc_1ao53b_nota,calc_1ao53b_recuperacao,calc_1ao54b_nota,calc_1ao54b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_1ao5pontos');
        var status      = $(this).closest('tr').find('.calc_1ao5status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_1ao5exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    /***************************************************/

    /**** BLOCO PARA O JAVASCRIPT DO BOLETIM 6 AO 8 ****/

    // Ao digitar nos campos do 1° Bimestre - Calcula a nota do primeiro bimestre e tenta calcular os pontos finais
    $( ".calc_6ao81b_mensal,.calc_6ao81b_bimestral,.calc_6ao81b_simulado,.calc_6ao81b_tarefa,.calc_6ao81b_extra,.calc_6ao81b_recuperacao" ).keyup(function( event ) {
        var calc_6ao81b_nota        = $(this).closest('tr').find('.calc_6ao81b_nota');
        var calc_6ao81b_mensal      = parseFloat($(this).closest('tr').find('.calc_6ao81b_mensal').val());
        var calc_6ao81b_bimestral   = parseFloat($(this).closest('tr').find('.calc_6ao81b_bimestral').val());
        var calc_6ao81b_simulado    = parseFloat($(this).closest('tr').find('.calc_6ao81b_simulado').val());
        var calc_6ao81b_tarefa      = parseFloat($(this).closest('tr').find('.calc_6ao81b_tarefa').val());
        var calc_6ao81b_extra       = parseFloat($(this).closest('tr').find('.calc_6ao81b_extra').val());
        var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
        var resultado = calculoMedia6ao8(calc_6ao81b_simulado,calc_6ao81b_tarefa,calc_6ao81b_mensal,calc_6ao81b_bimestral,calc_6ao81b_extra,calc_6ao81b_recuperacao);

        if(resultado) {
            calc_6ao81b_nota.val(resultado);
            var calc_6ao81b_nota = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
            var calc_6ao82b_nota = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
            var calc_6ao83b_nota = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
            var calc_6ao84b_nota = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

            var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
            var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
            var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
            var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

            var calc_6ao8exame = parseFloat($(this).closest('tr').find('.calc_6ao8exame').val());

            var calc_pontos = calculoPontos(calc_6ao81b_nota, calc_6ao81b_recuperacao, calc_6ao82b_nota, calc_6ao82b_recuperacao, calc_6ao83b_nota, calc_6ao83b_recuperacao, calc_6ao84b_nota, calc_6ao84b_recuperacao);

            var pontos = $(this).closest('tr').find('.calc_6ao8pontos');
            var status = $(this).closest('tr').find('.calc_6ao8status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos, calc_6ao8exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_6ao81b_nota.val("");
        }
    });

    // Ao digitar nos campos do 2° Bimestre - Calcula a nota do segundo bimestre e tenta calcular os pontos finais
    $( ".calc_6ao82b_mensal,.calc_6ao82b_bimestral,.calc_6ao82b_simulado,.calc_6ao82b_tarefa,.calc_6ao82b_extra,.calc_6ao82b_recuperacao" ).keyup(function( event ) {
        var calc_6ao82b_nota        = $(this).closest('tr').find('.calc_6ao82b_nota');
        var calc_6ao82b_mensal      = parseFloat($(this).closest('tr').find('.calc_6ao82b_mensal').val());
        var calc_6ao82b_bimestral   = parseFloat($(this).closest('tr').find('.calc_6ao82b_bimestral').val());
        var calc_6ao82b_simulado    = parseFloat($(this).closest('tr').find('.calc_6ao82b_simulado').val());
        var calc_6ao82b_tarefa      = parseFloat($(this).closest('tr').find('.calc_6ao82b_tarefa').val());
        var calc_6ao82b_extra       = parseFloat($(this).closest('tr').find('.calc_6ao82b_extra').val());
        var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
        var resultado = calculoMedia6ao8(calc_6ao82b_simulado,calc_6ao82b_tarefa,calc_6ao82b_mensal,calc_6ao82b_bimestral,calc_6ao82b_extra,calc_6ao82b_recuperacao);

        if(resultado){
            calc_6ao82b_nota.val(resultado);
            var calc_6ao81b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
            var calc_6ao82b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
            var calc_6ao83b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
            var calc_6ao84b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

            var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
            var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
            var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
            var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

            var calc_6ao8exame          = parseFloat($(this).closest('tr').find('.calc_6ao8exame').val());

            var calc_pontos = calculoPontos(calc_6ao81b_nota,calc_6ao81b_recuperacao,calc_6ao82b_nota,calc_6ao82b_recuperacao,calc_6ao83b_nota,calc_6ao83b_recuperacao,calc_6ao84b_nota,calc_6ao84b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_6ao8pontos');
            var status      = $(this).closest('tr').find('.calc_6ao8status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_6ao8exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_6ao82b_nota.val("");
        }
    });

    // Ao digitar nos campos do 3° Bimestre - Calcula a nota do terceiro bimestre e tenta calcular os pontos finais
    $( ".calc_6ao83b_mensal,.calc_6ao83b_bimestral,.calc_6ao83b_simulado,.calc_6ao83b_tarefa,.calc_6ao83b_extra,.calc_6ao83b_recuperacao" ).keyup(function( event ) {
        var calc_6ao83b_nota        = $(this).closest('tr').find('.calc_6ao83b_nota');
        var calc_6ao83b_mensal      = parseFloat($(this).closest('tr').find('.calc_6ao83b_mensal').val());
        var calc_6ao83b_bimestral   = parseFloat($(this).closest('tr').find('.calc_6ao83b_bimestral').val());
        var calc_6ao83b_simulado    = parseFloat($(this).closest('tr').find('.calc_6ao83b_simulado').val());
        var calc_6ao83b_tarefa      = parseFloat($(this).closest('tr').find('.calc_6ao83b_tarefa').val());
        var calc_6ao83b_extra       = parseFloat($(this).closest('tr').find('.calc_6ao83b_extra').val());
        var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
        var resultado = calculoMedia6ao8(calc_6ao83b_simulado,calc_6ao83b_tarefa,calc_6ao83b_mensal,calc_6ao83b_bimestral,calc_6ao83b_extra,calc_6ao83b_recuperacao);

        if(resultado){
            calc_6ao83b_nota.val(resultado);
            var calc_6ao81b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
            var calc_6ao82b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
            var calc_6ao83b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
            var calc_6ao84b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

            var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
            var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
            var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
            var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

            var calc_6ao8exame          = parseFloat($(this).closest('tr').find('.calc_6ao8exame').val());

            var calc_pontos = calculoPontos(calc_6ao81b_nota,calc_6ao81b_recuperacao,calc_6ao82b_nota,calc_6ao82b_recuperacao,calc_6ao83b_nota,calc_6ao83b_recuperacao,calc_6ao84b_nota,calc_6ao84b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_6ao8pontos');
            var status      = $(this).closest('tr').find('.calc_6ao8status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_6ao8exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_6ao83b_nota.val("");
        }
    });

    // Ao digitar nos campos do 4° Bimestre - Calcula a nota do quarto bimestre e tenta calcular os pontos finais
    $( ".calc_6ao84b_mensal,.calc_6ao84b_bimestral,.calc_6ao84b_simulado,.calc_6ao84b_tarefa,.calc_6ao84b_extra,.calc_6ao84b_recuperacao" ).keyup(function( event ) {
        var calc_6ao84b_nota        = $(this).closest('tr').find('.calc_6ao84b_nota');
        var calc_6ao84b_mensal      = parseFloat($(this).closest('tr').find('.calc_6ao84b_mensal').val());
        var calc_6ao84b_bimestral   = parseFloat($(this).closest('tr').find('.calc_6ao84b_bimestral').val());
        var calc_6ao84b_simulado    = parseFloat($(this).closest('tr').find('.calc_6ao84b_simulado').val());
        var calc_6ao84b_tarefa      = parseFloat($(this).closest('tr').find('.calc_6ao84b_tarefa').val());
        var calc_6ao84b_extra       = parseFloat($(this).closest('tr').find('.calc_6ao84b_extra').val());
        var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());
        var resultado = calculoMedia6ao8(calc_6ao84b_simulado,calc_6ao84b_tarefa,calc_6ao84b_mensal,calc_6ao84b_bimestral,calc_6ao84b_extra,calc_6ao84b_recuperacao);

        if(resultado){
            calc_6ao84b_nota.val(resultado);
            var calc_6ao81b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
            var calc_6ao82b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
            var calc_6ao83b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
            var calc_6ao84b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

            var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
            var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
            var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
            var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

            var calc_6ao8exame          = parseFloat($(this).closest('tr').find('.calc_6ao8exame').val());

            var calc_pontos = calculoPontos(calc_6ao81b_nota,calc_6ao81b_recuperacao,calc_6ao82b_nota,calc_6ao82b_recuperacao,calc_6ao83b_nota,calc_6ao83b_recuperacao,calc_6ao84b_nota,calc_6ao84b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_6ao8pontos');
            var status      = $(this).closest('tr').find('.calc_6ao8status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_6ao8exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_6ao84b_nota.val("");
        }
    });

    // Ao digitar nos campos da recuperação (todos os bimestres) - Tenta calcular os pontos finais
    $( ".calc_6ao81b_recuperacao,.calc_6ao82b_recuperacao,.calc_6ao83b_recuperacao,.calc_6ao84b_recuperacao" ).keyup(function( event ) {
        var calc_6ao81b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
        var calc_6ao82b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
        var calc_6ao83b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
        var calc_6ao84b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

        var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
        var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
        var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
        var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

        var calc_6ao8exame          = parseFloat($(this).closest('tr').find('.calc_6ao8exame').val());

        var calc_pontos = calculoPontos(calc_6ao81b_nota,calc_6ao81b_recuperacao,calc_6ao82b_nota,calc_6ao82b_recuperacao,calc_6ao83b_nota,calc_6ao83b_recuperacao,calc_6ao84b_nota,calc_6ao84b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_6ao8pontos');
        var status      = $(this).closest('tr').find('.calc_6ao8status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_6ao8exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    // Ao digitar nos campos da exame (todos os bimestres) - Calcula o STATUS
    $( ".calc_6ao8exame" ).keyup(function( event ) {
        var calc_6ao81b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao81b_nota').val());
        var calc_6ao82b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao82b_nota').val());
        var calc_6ao83b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao83b_nota').val());
        var calc_6ao84b_nota        = parseFloat($(this).closest('tr').find('.calc_6ao84b_nota').val());

        var calc_6ao81b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao81b_recuperacao').val());
        var calc_6ao82b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao82b_recuperacao').val());
        var calc_6ao83b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao83b_recuperacao').val());
        var calc_6ao84b_recuperacao = parseFloat($(this).closest('tr').find('.calc_6ao84b_recuperacao').val());

        var calc_6ao8exame          = parseFloat($(this).val());

        var calc_pontos = calculoPontos(calc_6ao81b_nota,calc_6ao81b_recuperacao,calc_6ao82b_nota,calc_6ao82b_recuperacao,calc_6ao83b_nota,calc_6ao83b_recuperacao,calc_6ao84b_nota,calc_6ao84b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_6ao8pontos');
        var status      = $(this).closest('tr').find('.calc_6ao8status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_6ao8exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    /***************************************************/

    /**** BLOCO PARA O JAVASCRIPT DO BOLETIM 9 ****/

    // Ao digitar nos campos do 1° Bimestre - Calcula a nota do primeiro bimestre e tenta calcular os pontos finais
    $( ".calc_91b_p1,.calc_91b_p2,.calc_91b_simulado,.calc_91b_tarefa,.calc_91b_extra,.calc_91b_recuperacao" ).keyup(function( event ) {
        var calc_91b_nota       = $(this).closest('tr').find('.calc_91b_nota');
        var calc_91b_p1         = parseFloat($(this).closest('tr').find('.calc_91b_p1').val());
        var calc_91b_p2         = parseFloat($(this).closest('tr').find('.calc_91b_p2').val());
        var calc_91b_simulado   = parseFloat($(this).closest('tr').find('.calc_91b_simulado').val());
        var calc_91b_tarefa     = parseFloat($(this).closest('tr').find('.calc_91b_tarefa').val());
        var calc_91b_extra      = parseFloat($(this).closest('tr').find('.calc_91b_extra').val());
        var calc_91b_recuperacao= parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
        var resultado           = calculoMedia9(calc_91b_simulado,calc_91b_tarefa,calc_91b_p1,calc_91b_p2,calc_91b_extra,calc_91b_recuperacao);

        if(resultado){
            calc_91b_nota.val(resultado);
            var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
            var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
            var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
            var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

            var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
            var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
            var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
            var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

            var calc_9exame          = parseFloat($(this).closest('tr').find('.calc_9exame').val());

            var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_9pontos');
            var status      = $(this).closest('tr').find('.calc_9status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_9exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_91b_nota.val("");
        }
    });

    // Ao digitar nos campos do 2° Bimestre - Calcula a nota do segundo bimestre e tenta calcular os pontos finais
    $( ".calc_92b_p1,.calc_92b_p2,.calc_92b_simulado,.calc_92b_tarefa,.calc_92b_extra,.calc_92b_recuperacao" ).keyup(function( event ) {
        var calc_92b_nota       = $(this).closest('tr').find('.calc_92b_nota');
        var calc_92b_p1         = parseFloat($(this).closest('tr').find('.calc_92b_p1').val());
        var calc_92b_p2         = parseFloat($(this).closest('tr').find('.calc_92b_p2').val());
        var calc_92b_simulado   = parseFloat($(this).closest('tr').find('.calc_92b_simulado').val());
        var calc_92b_tarefa     = parseFloat($(this).closest('tr').find('.calc_92b_tarefa').val());
        var calc_92b_extra      = parseFloat($(this).closest('tr').find('.calc_92b_extra').val());
        var calc_92b_recuperacao= parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
        var resultado           = calculoMedia9(calc_92b_simulado,calc_92b_tarefa,calc_92b_p1,calc_92b_p2,calc_92b_extra,calc_92b_recuperacao);

        if(resultado){
            calc_92b_nota.val(resultado);
            var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
            var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
            var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
            var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

            var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
            var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
            var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
            var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

            var calc_9exame          = parseFloat($(this).closest('tr').find('.calc_9exame').val());

            var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_9pontos');
            var status      = $(this).closest('tr').find('.calc_9status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_9exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_92b_nota.val("");
        }
    });

    // Ao digitar nos campos do 3° Bimestre - Calcula a nota do terceiro bimestre e tenta calcular os pontos finais
    $( ".calc_93b_p1,.calc_93b_p2,.calc_93b_simulado,.calc_93b_tarefa,.calc_93b_extra,.calc_93b_recuperacao" ).keyup(function( event ) {
        var calc_93b_nota       = $(this).closest('tr').find('.calc_93b_nota');
        var calc_93b_p1         = parseFloat($(this).closest('tr').find('.calc_93b_p1').val());
        var calc_93b_p2         = parseFloat($(this).closest('tr').find('.calc_93b_p2').val());
        var calc_93b_simulado   = parseFloat($(this).closest('tr').find('.calc_93b_simulado').val());
        var calc_93b_tarefa     = parseFloat($(this).closest('tr').find('.calc_93b_tarefa').val());
        var calc_93b_extra      = parseFloat($(this).closest('tr').find('.calc_93b_extra').val());
        var calc_93b_recuperacao= parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
        var resultado           = calculoMedia9(calc_93b_simulado,calc_93b_tarefa,calc_93b_p1,calc_93b_p2,calc_93b_extra,calc_93b_recuperacao);

        if(resultado){
            calc_93b_nota.val(resultado);
            var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
            var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
            var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
            var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

            var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
            var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
            var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
            var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

            var calc_9exame          = parseFloat($(this).closest('tr').find('.calc_9exame').val());

            var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_9pontos');
            var status      = $(this).closest('tr').find('.calc_9status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_9exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_93b_nota.val("");
        }
    });

    // Ao digitar nos campos do 4° Bimestre - Calcula a nota do quarto bimestre e tenta calcular os pontos finais
    $( ".calc_94b_p1,.calc_94b_p2,.calc_94b_simulado,.calc_94b_tarefa,.calc_94b_extra,.calc_94b_recuperacao" ).keyup(function( event ) {
        var calc_94b_nota       = $(this).closest('tr').find('.calc_94b_nota');
        var calc_94b_p1         = parseFloat($(this).closest('tr').find('.calc_94b_p1').val());
        var calc_94b_p2         = parseFloat($(this).closest('tr').find('.calc_94b_p2').val());
        var calc_94b_simulado   = parseFloat($(this).closest('tr').find('.calc_94b_simulado').val());
        var calc_94b_tarefa     = parseFloat($(this).closest('tr').find('.calc_94b_tarefa').val());
        var calc_94b_extra      = parseFloat($(this).closest('tr').find('.calc_94b_extra').val());
        var calc_94b_recuperacao= parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());
        var resultado           = calculoMedia9(calc_94b_simulado,calc_94b_tarefa,calc_94b_p1,calc_94b_p2,calc_94b_extra,calc_94b_recuperacao);

        if(resultado){
            calc_94b_nota.val(resultado);
            var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
            var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
            var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
            var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

            var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
            var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
            var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
            var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

            var calc_9exame          = parseFloat($(this).closest('tr').find('.calc_9exame').val());

            var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

            var pontos      = $(this).closest('tr').find('.calc_9pontos');
            var status      = $(this).closest('tr').find('.calc_9status');
            pontos.val(calc_pontos);

            var label_status = calculoStatus(calc_pontos,calc_9exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_94b_nota.val("");
        }
    });

    // Ao digitar nos campos da recuperação (todos os bimestres) - Tenta calcular os pontos finais
    $( ".calc_91b_recuperacao,.calc_92b_recuperacao,.calc_93b_recuperacao,.calc_94b_recuperacao" ).keyup(function( event ) {
        var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
        var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
        var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
        var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

        var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
        var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
        var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
        var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

        var calc_9exame          = parseFloat($(this).closest('tr').find('.calc_9exame').val());

        var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_9pontos');
        var status      = $(this).closest('tr').find('.calc_9status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_9exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    // Ao digitar nos campos da exame (todos os bimestres) - Calcula o STATUS
    $( ".calc_9exame" ).keyup(function( event ) {
        var calc_91b_nota        = parseFloat($(this).closest('tr').find('.calc_91b_nota').val());
        var calc_92b_nota        = parseFloat($(this).closest('tr').find('.calc_92b_nota').val());
        var calc_93b_nota        = parseFloat($(this).closest('tr').find('.calc_93b_nota').val());
        var calc_94b_nota        = parseFloat($(this).closest('tr').find('.calc_94b_nota').val());

        var calc_91b_recuperacao = parseFloat($(this).closest('tr').find('.calc_91b_recuperacao').val());
        var calc_92b_recuperacao = parseFloat($(this).closest('tr').find('.calc_92b_recuperacao').val());
        var calc_93b_recuperacao = parseFloat($(this).closest('tr').find('.calc_93b_recuperacao').val());
        var calc_94b_recuperacao = parseFloat($(this).closest('tr').find('.calc_94b_recuperacao').val());

        var calc_9exame          = parseFloat($(this).val());

        var calc_pontos = calculoPontos(calc_91b_nota,calc_91b_recuperacao,calc_92b_nota,calc_92b_recuperacao,calc_93b_nota,calc_93b_recuperacao,calc_94b_nota,calc_94b_recuperacao);

        var pontos      = $(this).closest('tr').find('.calc_9pontos');
        var status      = $(this).closest('tr').find('.calc_9status');
        pontos.val(calc_pontos);

        var label_status = calculoStatus(calc_pontos,calc_9exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
    });

    /***************************************************/

    /**** BLOCO PARA O JAVASCRIPT DO BOLETIM 1 ao 2 ENSINO MÉDIO ****/

    // Ao digitar nos campos do 1° Bimestre - Calcula a nota do primeiro bimestre e tenta calcular os pontos finais
    $( ".calc_1ao21b_p1,.calc_1ao21b_p2,.calc_1ao21b_simulado,.calc_1ao21b_recuperacao" ).keyup(function( event ) {
        var calc_1ao21b_nota        = $(this).closest('tr').find('.calc_1ao21b_nota');
        var calc_1ao21b_p1          = parseFloat($(this).closest('tr').find('.calc_1ao21b_p1').val());
        var calc_1ao21b_p2          = parseFloat($(this).closest('tr').find('.calc_1ao21b_p2').val());
        var calc_1ao21b_simulado    = parseFloat($(this).closest('tr').find('.calc_1ao21b_simulado').val());
        var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
        var calc_1ao21b_nota_materia= $(this).parents(".table").find('.calc_1ao21b_nota_materia');
        var resultado               = calculoMedia1ao2(calc_1ao21b_simulado,calc_1ao21b_p1,calc_1ao21b_p2,calc_1ao21b_recuperacao);
        //calculoMediaGeral1b(calc_1ao21b_nota_materia);

        if(resultado){
            calc_1ao21b_nota.val(resultado);
            var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
            var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
            var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
            var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

            var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
            var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
            var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
            var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

            var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

            var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
            var status      = $(this).closest('tr').find('.calc_1ao2status');

            var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
            pontos.val(calc_pontos);

            var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao21b_nota.val("");
        }
    });

    // Ao digitar nos campos do 2° Bimestre - Calcula a nota do segundo bimestre e tenta calcular os pontos finais
    $( ".calc_1ao22b_p1,.calc_1ao22b_p2,.calc_1ao22b_simulado,.calc_1ao22b_recuperacao" ).keyup(function( event ) {
        var calc_1ao22b_nota        = $(this).closest('tr').find('.calc_1ao22b_nota');
        var calc_1ao22b_p1          = parseFloat($(this).closest('tr').find('.calc_1ao22b_p1').val());
        var calc_1ao22b_p2          = parseFloat($(this).closest('tr').find('.calc_1ao22b_p2').val());
        var calc_1ao22b_simulado    = parseFloat($(this).closest('tr').find('.calc_1ao22b_simulado').val());
        var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
        var calc_1ao22b_nota_materia= $(this).parents(".table").find('.calc_1ao22b_nota_materia');
        var resultado               = calculoMedia1ao2(calc_1ao22b_simulado,calc_1ao22b_p1,calc_1ao22b_p2,calc_1ao22b_recuperacao);
        //calculoMediaGeral2b(calc_1ao22b_nota_materia);

        if(resultado){
            calc_1ao22b_nota.val(resultado);
            var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
            var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
            var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
            var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

            var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
            var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
            var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
            var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

            var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

            var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
            var status      = $(this).closest('tr').find('.calc_1ao2status');

            var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
            pontos.val(calc_pontos);

            var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao22b_nota.val("");
        }
    });

    // Ao digitar nos campos do 3° Bimestre - Calcula a nota do terceiro bimestre e tenta calcular os pontos finais
    $( ".calc_1ao23b_p1,.calc_1ao23b_p2,.calc_1ao23b_simulado,.calc_1ao23b_recuperacao" ).keyup(function( event ) {
        var calc_1ao23b_nota        = $(this).closest('tr').find('.calc_1ao23b_nota');
        var calc_1ao23b_p1          = parseFloat($(this).closest('tr').find('.calc_1ao23b_p1').val());
        var calc_1ao23b_p2          = parseFloat($(this).closest('tr').find('.calc_1ao23b_p2').val());
        var calc_1ao23b_simulado    = parseFloat($(this).closest('tr').find('.calc_1ao23b_simulado').val());
        var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
        var calc_1ao23b_nota_materia= $(this).parents(".table").find('.calc_1ao23b_nota_materia');
        var resultado               = calculoMedia1ao2(calc_1ao23b_simulado,calc_1ao23b_p1,calc_1ao23b_p2,calc_1ao23b_recuperacao);
        //calculoMediaGeral3b(calc_1ao23b_nota_materia);

        if(resultado){
            calc_1ao23b_nota.val(resultado);
            var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
            var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
            var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
            var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

            var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
            var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
            var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
            var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

            var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

            var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
            var status      = $(this).closest('tr').find('.calc_1ao2status');

            var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
            pontos.val(calc_pontos);

            var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao23b_nota.val("");
        }
    });

    // Ao digitar nos campos do 3° Bimestre - Calcula a nota do quarto bimestre e tenta calcular os pontos finais
    $( ".calc_1ao24b_p1,.calc_1ao24b_p2,.calc_1ao24b_simulado,.calc_1ao24b_recuperacao" ).keyup(function( event ) {
        var calc_1ao24b_nota        = $(this).closest('tr').find('.calc_1ao24b_nota');
        var calc_1ao24b_p1          = parseFloat($(this).closest('tr').find('.calc_1ao24b_p1').val());
        var calc_1ao24b_p2          = parseFloat($(this).closest('tr').find('.calc_1ao24b_p2').val());
        var calc_1ao24b_simulado    = parseFloat($(this).closest('tr').find('.calc_1ao24b_simulado').val());
        var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());
        var calc_1ao24b_nota_materia= $(this).parents(".table").find('.calc_1ao24b_nota_materia');
        var resultado               = calculoMedia1ao2(calc_1ao24b_simulado,calc_1ao24b_p1,calc_1ao24b_p2,calc_1ao24b_recuperacao);
        //calculoMediaGeral4b(calc_1ao24b_nota_materia);

        if(resultado){
            calc_1ao24b_nota.val(resultado);
            var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
            var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
            var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
            var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

            var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
            var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
            var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
            var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

            var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

            var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
            var status      = $(this).closest('tr').find('.calc_1ao2status');

            var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
            pontos.val(calc_pontos);

            var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
            status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        }else{
            calc_1ao24b_nota.val("");
        }
    });

    // Ao digitar nos campos da recuperação (todos os bimestres) - Tenta calcular os pontos finais
    $( ".calc_1ao21b_recuperacao,.calc_1ao22b_recuperacao,.calc_1ao23b_recuperacao,.calc_1ao24b_recuperacao" ).keyup(function( event ) {
        var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
        var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
        var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
        var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

        var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
        var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
        var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
        var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

        var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

        var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
        var status      = $(this).closest('tr').find('.calc_1ao2status');

        var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
        pontos.val(calc_pontos);

        var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        console.log(label_status);
    });

    // Ao digitar no campo de exame - Tenta calcular os pontos finais
    $( ".calc_1ao2exame" ).keyup(function( event ) {
        var calc_1ao21b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao21b_nota').val());
        var calc_1ao22b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao22b_nota').val());
        var calc_1ao23b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao23b_nota').val());
        var calc_1ao24b_nota        = parseFloat($(this).closest('tr').find('.calc_1ao24b_nota').val());

        var calc_1ao21b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao21b_recuperacao').val());
        var calc_1ao22b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao22b_recuperacao').val());
        var calc_1ao23b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao23b_recuperacao').val());
        var calc_1ao24b_recuperacao = parseFloat($(this).closest('tr').find('.calc_1ao24b_recuperacao').val());

        var calc_1ao2exame          = parseFloat($(this).closest('tr').find('.calc_1ao2exame').val());

        var calc_pontos = calculoPontosEM(calc_1ao21b_nota,calc_1ao21b_recuperacao,calc_1ao22b_nota,calc_1ao22b_recuperacao,calc_1ao23b_nota,calc_1ao23b_recuperacao,calc_1ao24b_nota,calc_1ao24b_recuperacao);
        var status      = $(this).closest('tr').find('.calc_1ao2status');

        var pontos      = $(this).closest('tr').find('.calc_1ao2pontos');
        pontos.val(calc_pontos);

        var label_status = calculoStatusEM(calc_pontos,calc_1ao2exame);
        status.removeClass('APROVADO').removeClass('REPROVADO').addClass(label_status).html(label_status);
        console.log(label_status);
    });

    /***************************************************/

})(jQuery);

/*!
* based on formNavigation https://github.com/omichelsen/FormNavigation
*/
(function ($) {
    $.fn.formNavigation = function () {
        $(this).each(function () {
            // Events triggered on keyup
            $(this).find('input').on('keyup', function(e) {
                switch (e.which) {
                    // arrow right
                    case 39:
                        $(this).closest('td').next().find('input').focus();
                        break;

                    // arrow left
                    case 37:
                        $(this).closest('td').prev().find('input').focus();
                        break;

                    // arrow bottom
                    case 40:
                        $(this).closest('tr').next().children().eq($(this).closest('td').index()).find('input').focus();
                        break;

                    // arrow top
                    case 38:
                        $(this).closest('tr').prev().children().eq($(this).closest('td').index()).find('input').focus();
                        break;

                    // enter
                    case 13:
                        if ($(this).closest('td').next().find('input').length>0) {
                            // when there is another column on right
                            $(this).closest('td').next().find('input').focus();
                        } else {
                            // when last column reached
                            $(this).closest('tr').next().children().eq(1).find('input').focus();
                        }
                        break;
                }
            });

            // Events triggered on keydown (repeatable when holding the key)
            $(this).find('input').on('keydown', function(e) {
                // Vertical navigation using tab as OP wanted
                if (e.which === 9 && !e.shiftKey) {
                    // navigate forward
                    if ($(this).closest('tr').next().find('input').length>0) {
                        // when there is another row below
                        e.preventDefault();
                        $(this).closest('tr').next().children().eq($(this).closest('td').index()).find('input').focus();
                    } else if ($(this).closest('tbody').find('tr:first').children().eq($(this).closest('td').index()+1).find('input').length>0) {
                        // when last row reached
                        e.preventDefault();
                        $(this).closest('tbody').find('tr:first').children().eq($(this).closest('td').index()+1).find('input').focus();
                    }
                } else if (e.which === 9 && e.shiftKey) {
                    // navigate backward
                    if ($(this).closest('tr').prev().find('input').length>0) {
                        // when there is another row above
                        e.preventDefault();
                        $(this).closest('tr').prev().children().eq($(this).closest('td').index()).find('input').focus();
                    } else if ($(this).closest('tbody').find('tr:last').children().eq($(this).closest('td').index()-1).find('input').length>0) {
                        // when first row reached
                        e.preventDefault();
                        $(this).closest('tbody').find('tr:last').children().eq($(this).closest('td').index()-1).find('input').focus();
                    }
                }
            });
        });
    };
})(jQuery);

function calculoMedia1ao5(calc_1ao5nam,calc_1ao5mensal,calc_1ao5bimestral,calc_1ao5extra,calc_1ao5recuperacao){
    if(!isNaN(calc_1ao5nam) && !isNaN(calc_1ao5mensal) && !isNaN(calc_1ao5bimestral)){
        if(!isNaN(calc_1ao5extra))
            var calculo = MRound((((calc_1ao5nam+calc_1ao5mensal+calc_1ao5bimestral)/3)+calc_1ao5extra));
        else
            var calculo = MRound(((calc_1ao5nam+calc_1ao5mensal+calc_1ao5bimestral)/3));

        if(!isNaN(calc_1ao5recuperacao)){
            var media = (calculo>=calc_1ao5recuperacao) ? calculo : calc_1ao5recuperacao;
            return (media>7) ? "7.00" : media;
        }else
            return (calculo>10) ? "10.00" : calculo;
    }else{
        return "";
    }
}
function calculoMedia6ao8(calc_6ao8simulado,calc_6ao8tarefa,calc_6ao8mensal,calc_6ao8bimestral,calc_6ao8extra,calc_6ao8recuperacao){
    if(!isNaN(calc_6ao8simulado) && !isNaN(calc_6ao8tarefa) && !isNaN(calc_6ao8mensal) && !isNaN(calc_6ao8bimestral)){
        if(!isNaN(calc_6ao8extra))
            var calculo = MRound(((((calc_6ao8simulado+calc_6ao8tarefa)+calc_6ao8mensal+calc_6ao8bimestral)/3)+calc_6ao8extra));
        else
            var calculo = MRound((((calc_6ao8simulado+calc_6ao8tarefa)+calc_6ao8mensal+calc_6ao8bimestral)/3));

        if(!isNaN(calc_6ao8recuperacao)){
            var media = (calculo>=calc_6ao8recuperacao) ? calculo : calc_6ao8recuperacao;
            return (media>7) ? "7.00" : media;
        }else
            return (calculo>10) ? "10.00" : calculo;
    }else{
        return "";
    }
}
function calculoMedia9(calc_9simulado,calc_9tarefa,calc_9p1,calc_9p2,calc_9extra,calc_9recuperacao){
    if(!isNaN(calc_9simulado) && !isNaN(calc_9tarefa) && !isNaN(calc_9p1) && !isNaN(calc_9p2)){
        if(!isNaN(calc_9extra))
            var calculo = MRound(((((calc_9simulado+calc_9tarefa)+calc_9p1+calc_9p2)/3)+calc_9extra));
        else
            var calculo = MRound((((calc_9simulado+calc_9tarefa)+calc_9p1+calc_9p2)/3));

        if(!isNaN(calc_9recuperacao)){
            var media = (calculo>=calc_9recuperacao) ? calculo : calc_9recuperacao;
            return (media>7) ? "7.00" : media;
        }else
            return (calculo>10) ? "10.00" : calculo;
    }else{
        return "";
    }
}
function calculoMedia1ao2(calc_1ao2simulado,calc_1ao2p1,calc_1ao2p2,calc_1ao2recuperacao){
    if(!isNaN(calc_1ao2simulado) && !isNaN(calc_1ao2p1) && !isNaN(calc_1ao2p2)){
        var calculo = MRound(((calc_1ao2p1*0.4)+(calc_1ao2p2*0.4)+(calc_1ao2simulado)));

        if(!isNaN(calc_1ao2recuperacao)){
            var media = (calculo>=calc_1ao2recuperacao) ? calculo : calc_1ao2recuperacao;
            return (media>6) ? "6.00" : media;
        }else
            return (calculo>10) ? "10.00" : calculo;
    }else{
        return "";
    }
}

function calculoMediaGeral1b(calc_1ao2_nota_materia){
    for (variavel = 0; variavel <= 7; variavel++) {
        var todos_preenchidos = {};
        var valor_total = 0;
        var i = 0;
        todos_preenchidos[variavel] = true;
        $(".abrir"+variavel+" .calc_1ao21b_nota").each(function (index) {
            if (!$(this).val()) {
                todos_preenchidos[variavel] = false;
            } else {
                console.log(index + ": " + $(this).val());
                valor_total = valor_total + parseFloat($(this).val());
            }
            i = i + 1;
        });
        console.log("todos_preenchidos[variavel]: "+todos_preenchidos[variavel]);
        if (todos_preenchidos[variavel]) {
            valor_total = valor_total / i;
            console.log("valor_total" + " : " + (valor_total));
            if(!isNaN(valor_total)){
                console.log(calc_1ao2_nota_materia);
                $(calc_1ao2_nota_materia).each(function (index) {
                    console.log($(this));
                    if(index==variavel){
                        var calculo = MRound(valor_total);
                        $(this).val((calculo>10) ? "10.00" : calculo);
                    }
                });
            }
        }
    }
}
function calculoMediaGeral2b(calc_1ao2_nota_materia){
    for (variavel = 0; variavel <= 7; variavel++) {
        var todos_preenchidos = {};
        var valor_total = 0;
        var i = 0;
        todos_preenchidos[variavel] = true;
        $(".abrir"+variavel+" .calc_1ao22b_nota").each(function (index) {
            if (!$(this).val()) {
                todos_preenchidos[variavel] = false;
            } else {
                console.log(index + ": " + $(this).val());
                valor_total = valor_total + parseFloat($(this).val());
            }
            i = i + 1;
        });
        console.log("todos_preenchidos[variavel]: "+todos_preenchidos[variavel]);
        if (todos_preenchidos[variavel]) {
            valor_total = valor_total / i;
            console.log("valor_total" + " : " + (valor_total));
            if(!isNaN(valor_total)){
                console.log(calc_1ao2_nota_materia);
                $(calc_1ao2_nota_materia).each(function (index) {
                    console.log($(this));
                    if(index==variavel){
                        var calculo = MRound(valor_total);
                        $(this).val((calculo>10) ? "10.00" : calculo);
                    }
                });
            }
        }
    }
}
function calculoMediaGeral3b(calc_1ao2_nota_materia){
    for (variavel = 0; variavel <= 7; variavel++) {
        var todos_preenchidos = {};
        var valor_total = 0;
        var i = 0;
        todos_preenchidos[variavel] = true;
        $(".abrir"+variavel+" .calc_1ao23b_nota").each(function (index) {
            if (!$(this).val()) {
                todos_preenchidos[variavel] = false;
            } else {
                console.log(index + ": " + $(this).val());
                valor_total = valor_total + parseFloat($(this).val());
            }
            i = i + 1;
        });
        console.log("todos_preenchidos[variavel]: "+todos_preenchidos[variavel]);
        if (todos_preenchidos[variavel]) {
            valor_total = valor_total / i;
            console.log("valor_total" + " : " + (valor_total));
            if(!isNaN(valor_total)){
                console.log(calc_1ao2_nota_materia);
                $(calc_1ao2_nota_materia).each(function (index) {
                    console.log($(this));
                    if(index==variavel){
                        var calculo = MRound(valor_total);
                        $(this).val((calculo>10) ? "10.00" : calculo);
                    }
                });
            }
        }
    }
}
function calculoMediaGeral4b(calc_1ao2_nota_materia){
    for (variavel = 0; variavel <= 7; variavel++) {
        var todos_preenchidos = {};
        var valor_total = 0;
        var i = 0;
        todos_preenchidos[variavel] = true;
        $(".abrir"+variavel+" .calc_1ao24b_nota").each(function (index) {
            if (!$(this).val()) {
                todos_preenchidos[variavel] = false;
            } else {
                console.log(index + ": " + $(this).val());
                valor_total = valor_total + parseFloat($(this).val());
            }
            i = i + 1;
        });
        console.log("todos_preenchidos[variavel]: "+todos_preenchidos[variavel]);
        if (todos_preenchidos[variavel]) {
            valor_total = valor_total / i;
            console.log("valor_total" + " : " + (valor_total));
            if(!isNaN(valor_total)){
                console.log(calc_1ao2_nota_materia);
                $(calc_1ao2_nota_materia).each(function (index) {
                    console.log($(this));
                    if(index==variavel){
                        var calculo = MRound(valor_total);
                        $(this).val((calculo>10) ? "10.00" : calculo);
                    }
                });
            }
        }
    }
}

function calculoPontos(calc_1b_nota,calc_1b_recuperacao,calc_2b_nota,calc_2b_recuperacao,calc_3b_nota,calc_3b_recuperacao,calc_4b_nota,calc_4b_recuperacao){
    if(!isNaN(calc_1b_nota)&&!isNaN(calc_2b_nota)&&!isNaN(calc_3b_nota)&&!isNaN(calc_4b_nota)){
        return MRound((calc_1b_nota*1)+(calc_2b_nota*2)+(calc_3b_nota*2)+(calc_4b_nota*2));
    }else{
        return "";
    }
}
function calculoStatus(pontos,exame){
    /*console.log("calculostatus pontos: "+pontos);
    console.log("calculostatus exame: "+exame);*/
    if(isNaN(pontos) || pontos === ""){
        return "";
    }
    if(isNaN(exame)){
        return (pontos >= 49) ? 'APROVADO' : 'REPROVADO';
    }else{
        if(pontos >= 49){
            return 'APROVADO';
        }else{
            return (exame >= ((50-pontos)/3)) ? 'APROVADO' : 'REPROVADO';
        }
    }
}

function calculoPontosEM(calc_1b_nota,calc_1b_recuperacao,calc_2b_nota,calc_2b_recuperacao,calc_3b_nota,calc_3b_recuperacao,calc_4b_nota,calc_4b_recuperacao){
    if(!isNaN(calc_1b_nota)&&!isNaN(calc_2b_nota)&&!isNaN(calc_3b_nota)&&!isNaN(calc_4b_nota)){
        return MRound((calc_1b_nota+calc_2b_nota+calc_3b_nota+calc_4b_nota)/4);
    }else{
        return "";
    }
}
function calculoStatusEM(pontos,exame){
    /*console.log("calculostatus pontos: "+pontos);
    console.log("calculostatus exame: "+exame);*/
    if(isNaN(pontos) || pontos === ""){
        return "";
    }
    var nota = (isNaN(exame)||pontos>exame) ? pontos : exame;
    return (nota >= 6) ? 'APROVADO' : 'REPROVADO';
}

function MRound(num) {
    /* Antiga formatacao das notas
    (Math.round((num) * 4) / 4).toFixed(2) */
    return num.toString().match(/^-?\d+(?:\.\d{0,2})?/)[0];
}