<?php
if(!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @param $string
 * @return string
 *
 */
function slug($string) {
    return strtolower(trim(preg_replace('~[^0-9a-z]+~i', '-', html_entity_decode(preg_replace('~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', htmlentities($string, ENT_QUOTES, 'UTF-8')), ENT_QUOTES, 'UTF-8')), '-'));
}

/**
 * @param $string
 * @param $int
 * @return string
 *
 */
function formatBytes($bytes, $precision = 2) {
    $units = array('B', 'KB', 'MB', 'GB', 'TB');

    $bytes = max($bytes, 0);
    $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
    $pow = min($pow, count($units) - 1);

    // Uncomment one of the following alternatives
    $bytes /= pow(1024, $pow);
    // $bytes /= (1 << (10 * $pow));

    return round($bytes, $precision) . ' ' . $units[$pow];
}

/**
 * Transformar numero da semana/ano em data inicial e final
 * @param $week
 * @param $year
 * @return array
 *
 */
function getWeek($week, $year) {
    $dto = new DateTime();
    $dto->setISODate($year, $week);
    $ret['week_start'] = $dto->format('d/m/Y');
    $dto->modify('+6 days');
    $ret['week_end'] = $dto->format('d/m/Y');
    return $ret;
}

/**
 * Deleta imagem da galeria
 * @param $img
 *
 */
function deleteImg($img)
{
    $arquivo = './public/img/'.$img;
    $arquivo_thumb = './public/img/thumbnail/'.$img;
    unlink($arquivo);
    unlink($arquivo_thumb);
}

/**
 * Grava log do ultimo comando do banco de dados junto com as credenciais de quem efetuou
 */
function gravaLog()
{
    $CI = &get_instance();
    $CI->load->model('logmd');

    $data = array(
        'id_responsavel' => $CI->session->id,
        'email_responsavel' => $CI->session->email,
        'nome_responsavel' => $CI->session->nome,
        'query' => $CI->db->last_query()
    );

    $CI->logmd->insertLog($data);
}

/**
 * Funcao que adiciona o http na url
 * @param $url
 * @return string
 *
 */
function addhttp($url) {
    if(!$url) {
        return "";
    }

    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
        $url = "http://" . $url;
    }
    return $url;
}