<?php $this->load->view('admin/header'); ?>
<!--START PAGE CONTENT -->
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase">Editar Perfil</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form class="form-horizontal" id="formulario" method="post" action="<?php echo base_url('index.php/perfilctrl/updateperfil');?>">
                        <div class="card-body">

                            <div class="form-body">
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Nome</label>
                                    <div class="col-md-5">
                                        <input type="hidden" name="id" class="form-control" value="<?=$perfil[0]->id?>" required>
                                        <input type="text" name="nome" class="form-control" value="<?=$perfil[0]->nome?>" placeholder="Obrigatório" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">E-mail</label>
                                    <div class="col-md-5">
                                        <input type="email" name="email" class="form-control" value="<?=$perfil[0]->email?>" placeholder="Obrigatório" required>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">CPF</label>
                                    <div class="col-md-5">
                                        <input type="text" name="cpf" id="cpf" class="form-control" value="<?=$perfil[0]->cpf?>" placeholder="Opcional">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Telefone</label>
                                    <div class="col-md-5">
                                        <input type="text" name="telefone" id="telefone" class="form-control" value="<?=$perfil[0]->telefone?>" placeholder="Opcional">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Celular</label>
                                    <div class="col-md-5">
                                        <input type="text" name="celular" id="celular" class="form-control" value="<?=$perfil[0]->celular?>" placeholder="Opcional">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Senha</label>
                                    <div class="col-md-5">
                                        <input type="password" name="senha" id="senha" class="form-control" value="" placeholder="Se não alterar a senha continua a mesma">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="control-label text-right col-md-4">Confirmar Senha</label>
                                    <div class="col-md-5">
                                        <input type="password" name="senha_confirmar" class="form-control" value="" placeholder="Confirmar a nova senha">
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md submit" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>

<script>
    var formatted = new Formatter(document.getElementById('telefone'), {
        'patterns': [
            {'*': '({{99}}) {{9999}}-{{9999}}'},
        ],
        'persistent': false
    });

    var formatted = new Formatter(document.getElementById('cpf'), {
        'patterns': [
            {'*': '{{999}}.{{999}}.{{999}}-{{99}}'},
        ],
        'persistent': false
    });

    var formatted = new Formatter(document.getElementById('celular'), {
        'patterns': [
            {'*': '({{99}}) {{99999}}-{{9999}}'},
        ],
        'persistent': false
    });
    $(document).ready(function() {
        $('#formulario').validate({
            rules: {
                senha_confirmar: {
                    required: function(element){
                        return $("#senha").val()!="";
                    },
                    equalTo: "#senha"
                }
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                return true;
            },
        });
    });
</script>
