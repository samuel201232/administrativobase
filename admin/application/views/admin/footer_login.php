
<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="<?=base_url();?>assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url();?>assets/vendor/js-storage/js.storage.js"></script>
<script src="<?=base_url();?>assets/vendor/js-cookie/src/js.cookie.js"></script>
<script src="<?=base_url();?>assets/vendor/pace/pace.js"></script>
<script src="<?=base_url();?>assets/vendor/metismenu/dist/metisMenu.js"></script>
<script src="<?=base_url();?>assets/vendor/switchery-npm/index.js"></script>
<script src="<?=base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="<?=base_url();?>assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery.validate.min.js"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="<?=base_url();?>assets/js/global/app.js"></script>

</body>

</html>