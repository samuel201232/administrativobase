<?php $menu['active1']="solucoescategoria";$menu['active2']="solucoessubcategoriactrl";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase"><?=isset($value[0]->id)?'Editar':'Criar'?> - Solucoes Sub Categoria</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form autocomplete="off" class="form-horizontal" id="formulario" method="post" action="<?=base_url('index.php/solucoessubcategoriactrl/insertOrUpdate');?>">
                        <input type="hidden" name="id" class="form-control" value="<?=isset($value[0]->id)?$value[0]->id:''?>">
                        <div class="tab-panel">
                            <ul class="nav nav-tabs info-tabs justify-content-center">
                                <li class="nav-item" role="presentation"><a href="#tab" class="nav-link active show" data-toggle="tab" aria-expanded="true"><i class="fas fa-align-left"></i> Conteúdo</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fadeIn active" id="tab">
                                    <!-- CAMPOS PORTUGUÊS -->
                                    <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Ativar / Desativar</label>
                                                <div class="col-md-5">
                                                    <input class="tgl tgl-light tgl-success" id="cb8" name="ativo" type="checkbox" <?=(!isset($value[0]->ativo)||$value[0]->ativo==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb8"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Categoria</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="id_categoria" id="id_categoria">
                                                        <option value="">-- Relacione a categoria --</option>
                                                        <?php foreach ($categorias as $categoria){
                                                            $select = ($categoria->id==$value[0]->id_categoria)?'selected':'';
                                                            echo '<option value="'.$categoria->id.'" '.$select.'>'.$categoria->titulo.'</option>';
                                                        } ?>
                                                    </select>
                                                </div>
											</div>

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Título</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="titulo" class="form-control" value="<?=isset($value[0]->titulo)?$value[0]->titulo:''?>" placeholder="Obrigatório" required>
                                                </div>
                                            </div>
											
                                        </div>
                                    </div>
                                    <!-- ---------------- -->
                                </div>

				

                            </div>
                        </div>

                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <?php if(isset($value[0]->id)) { ?>
                                                    <a role="button" class="deletar btn btn-danger btn-md">
                                                        <i class="far fa-trash-alt" style="color: inherit !important;"></i> Deletar
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/solucoessubcategoriactrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php if(isset($value[0]->id)) { ?>
    <form method="post" class="form-horizontal d-inline"  action="<?=base_url('index.php/solucoessubcategoriactrl/delete')?>" id="formulario_deletar">
        <input type="hidden" name="id" value="<?=$value[0]->id?>">
    </form>
<?php } ?>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script src="<?=base_url();?>assets/ckfinder/ckfinder.js"></script>
<script src="//cdn.ckeditor.com/ckeditor5/10.1.0/classic/translations/pt-br.js"></script>
<script>
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
        var to   = "aaaaaeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    $(document).ready(function() {

        /******* BOTAO DELETAR *******/
        $(".deletar").click(function(e) {
            e.preventDefault();
            var form = $("#formulario_deletar");
            swal({
                title: "Tem certeza que deseja deletar?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                cancelButtonText: "Não, quero cancelar",
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        form.submit();
                        setTimeout(function() {
                            resolve();
                        }, 2000);
                    });
                }
            })
        });
        /******* *******/

        /******* VALIDACAO FORMULARIO - SUBMIT *******/
        $('#formulario').validate({
            rules: {
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                return true;
            },
        });
        /******* *******/
    });
</script>
