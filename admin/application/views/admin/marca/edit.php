<?php $menu['active1']="marcacategoria";$menu['active2']="Marcactrl";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase"><?=isset($value[0]->id)?'Editar':'Criar'?> Marca</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form autocomplete="off" class="form-horizontal" id="formulario" method="post" action="<?=base_url('index.php/Marcactrl/insertOrUpdate');?>">
                        <input type="hidden" name="id" class="form-control" value="<?=isset($value[0]->id)?$value[0]->id:''?>">
                        <div class="tab-panel">
                            <ul class="nav nav-tabs info-tabs justify-content-center">
                                <li class="nav-item" role="presentation"><a href="#tab" class="nav-link active show" data-toggle="tab" aria-expanded="true"><i class="fas fa-align-left"></i> Conteúdo</a></li>
                                <li class="nav-item" role="presentation"><a href="#tab_img" class="nav-link" data-toggle="tab" aria-expanded="true"><i class="far fa-images"></i> Imagens</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fadeIn active" id="tab">
                                    <!-- CAMPOS PORTUGUÊS -->
                                    <div class="card-body">
                                        <div class="form-body">

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Ativar / Desativar</label>
                                                <div class="col-md-5">
                                                    <input class="tgl tgl-light tgl-success" id="cb8" name="ativo" type="checkbox" <?=(!isset($value[0]->ativo)||$value[0]->ativo==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb8"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
											</div>
											
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Título</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="titulo" class="form-control" value="<?=isset($value[0]->titulo)?$value[0]->titulo:''?>" placeholder="Obrigatório" required>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- ---------------- -->
                                </div>
                                <div class="tab-pane fadeIn" id="tab_img">
                                    <!-- CAMPOS IMAGENS -->


                                    
                                    <!-- ===== IMAGEM PRINCIPAL -->
                                    <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4 align-self-center">Imagem
                                                    principal </br> 566 x 566 px </br> 72dpi</label>
                                                <div class="col-md-5">
                                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="imagem_principal" class="form-control"
                                                        id="imagem_principal" style="display: none;"></br>
                                                    </br>
                                                    <div class="imagem_container_principal" style="width:300px;height:300px;border: 1px dashed; color:#ff8000;cursor:pointer" id="imagem_container_principal"
                                                        style="text-align: center">
                                                        <?php if(isset($value[0]->imagem_principal) && !empty($value[0]->imagem_principal)) { ?>
                                                            <img src="<?=base_url('public/'.$value[0]->imagem_principal)?>"
                                                                class="single-preview" id="img_principal_preview">        
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                               
                                    <!-- --------------- -->
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <?php if(isset($value[0]->id)) { ?>
                                                    <a role="button" class="deletar btn btn-danger btn-md">
                                                        <i class="far fa-trash-alt" style="color: inherit !important;"></i> Deletar
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/Marcactrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
<style>
    .single-preview {
        object-position: center;
        object-fit: contain;
        height: inherit;
        width: inherit;
    }

</style>
<?php if(isset($value[0]->id)) { ?>
    <form method="post" class="form-horizontal d-inline"  action="<?=base_url('index.php/Marcactrl/delete')?>" id="formulario_deletar">
        <input type="hidden" name="id" value="<?=$value[0]->id?>">
    </form>
<?php } ?>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<link rel="stylesheet" href="<?=base_url();?>assets/dropzone/min/basic.min.css" />
<link rel="stylesheet" href="<?=base_url();?>assets/dropzone/min/dropzone.min.css" />
<script src="<?=base_url();?>assets/dropzone/min/dropzone.min.js"></script>
<script src="<?=base_url();?>assets/dropzone/min/dropzone-amd-module.min.js"></script>
<script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script src="<?=base_url();?>assets/ckfinder/ckfinder.js"></script>
<script src="<?=base_url();?>assets/vendor/select2/select2.min.js"></script>
<script src="//cdn.ckeditor.com/ckeditor5/10.1.0/classic/translations/pt-br.js"></script>
<script>
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
        var to   = "aaaaaeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    $(document).ready(function() {

        /*******Imagem Principal *******/

        $('#imagem_container_principal').click(function() {
            $("input[id='imagem_principal']").click();
        });
        
        const imagem_principal = document.getElementById("imagem_principal");

        imagem_principal.addEventListener("change", function () {

            const file = this.files[0];

            if (file) {

                var imageTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types
                var extension = file.name.split('.').pop().toLowerCase();  //file extension from input file
                var isImage = imageTypes.indexOf(extension) > -1;  //is extension in acceptable types

                const reader = new FileReader();

                reader.addEventListener("load", function () {
                    console.log(this);
                    if ( $('#imagem_container_principal').children().length == 0 ) {
                        $('#imagem_container_principal').prepend('<img id="img_principal_preview" class="single-preview" src="" />')
                    }
                    
                    if(isImage){
                        document.querySelector("#img_principal_preview").setAttribute("src", this.result);
                    }
                    else{
                        document.querySelector("#img_principal_preview").setAttribute("src", '<?= base_url('public/admin/images/file-icon.png') ?>');
                    }

                });

                reader.readAsDataURL(file);
            }
        });
        /******* *******/

        /******* BOTAO DELETAR *******/
        $(".deletar").click(function(e) {
            e.preventDefault();
            var form = $("#formulario_deletar");
            swal({
                title: "Tem certeza que deseja deletar?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                cancelButtonText: "Não, quero cancelar",
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        form.submit();
                        setTimeout(function() {
                            resolve();
                        }, 2000);
                    });
                }
            })
        });
        /******* *******/

        /******* VALIDACAO FORMULARIO - SUBMIT *******/
        $('#formulario').validate({
            rules: {
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                return true;
            },
        });
        /******* *******/
    });
</script>
