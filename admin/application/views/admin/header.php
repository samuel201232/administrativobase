<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Área Administrativa</title>
    <!-- ================== GOOGLE FONTS ==================-->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500" rel="stylesheet">
    <link rel="stylesheet" href="//use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
    
    <!-- ======================= GLOBAL VENDOR STYLES ========================-->
    <link rel="stylesheet" href="<?=base_url();?>assets/css/vendor/bootstrap.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/metismenu/dist/metisMenu.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/switchery-npm/index.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/select2/select2.min.css" />
    <link href="<?=base_url();?>assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="<?=base_url();?>assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/layouts/vertical/core/main.css">
    <link href="<?=base_url();?>assets/css/sb-admin-2.min.css" rel="stylesheet">  
    <link rel="stylesheet" href="<?=base_url();?>assets/css/common/main.bundle.css"> 
    <link rel="stylesheet" href="<?=base_url();?>assets/css/layouts/vertical/themes/theme-a.css">
    <link rel="stylesheet" href="<?=base_url();?>assets/css/layouts/vertical/menu-type/default.css">      
   
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">Área Administrativa</div>
            </a>



            <h5 class="text-center"><?=$this->session->nome?></h5>
                <span class="text-center"><?=$this->session->email?></span>
            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Dashboard -->
            <li class="nav-item active">
                <a class="nav-link" href="index.html">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>Dashboard</span></a>
            </li>
            
            <!-- Divider -->
            <hr class="sidebar-divider">

            <!-- Heading -->
            <div class="sidebar-heading">
                Interface
            </div>

                                   <!-- Nav Item - Pages Collapse Menu  -->

                       

            <li class="nav-item">
                <a class="nav-link collapsed" href="<?=base_url('index.php/produtoctrl')?>" data-toggle="collapse" data-target="#collapseOne"
                    aria-expanded="true" aria-controls="collapseOne">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Produtos</span>
                </a>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">                        
                        <a class="collapse-item" href="<?=base_url('index.php/produtocategoriactrl')?>">Categorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/produtosubcategoriactrl')?>">Subcategorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/produtoctrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>

                        <!-- Nav Item - Utilities Collapse Menu -->
                                            <!--Pecas-->

           <li class="nav-item">
                <a class="nav-link collapsed" href="<?=base_url('index.php/Pecasctrl')?>" data-toggle="collapse" data-target="#collapseTwoo"
                    aria-expanded="true" aria-controls="collapseTwoo">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Pecas</span>
                </a>
                <div id="collapseTwoo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        
                        <a class="collapse-item" href="<?=base_url('index.php/pecascategoriactrl"')?>">Categorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/pecassubcategoriactrl')?>">Subcategorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/Pecasctrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>


                              <!-- Nav Item - Utilities Collapse Menu -->
                                            <!--Soluções-->

           <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('index.php/Solucoesctrl')?>" data-toggle="collapse" data-target="#collapseThree"
                    aria-expanded="true" aria-controls="collapseThree">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Soluções</span>
                </a>
                <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        
                        <a class="collapse-item" href="<?=base_url('index.php/solucoescategoriactrl')?>">Categorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/solucoessubcategoriactrl')?>">Subcategorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/Solucoesctrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>

                              <!-- Nav Item - Utilities Collapse Menu -->
                                            <!--Notícias-->

                     <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('index.php/noticiactrl')?>" data-toggle="collapse" data-target="#collapseFour"
                    aria-expanded="true" aria-controls="collapseFour">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Notícias</span>
                </a>
                <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        
                        <a class="collapse-item" href="utilities-color.html">Categorias</a>
                        <a class="collapse-item" href="utilities-border.html">Subcategorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/noticiactrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>


                              <!-- Nav Item - Utilities Collapse Menu -->
                                            <!--Banner-->

                <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('index.php/Bannerctrl')?>" data-toggle="collapse" data-target="#collapseFive"
                    aria-expanded="true" aria-controls="collapseFive">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Banner</span>
                </a>
                <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        
                        <a class="collapse-item" href="utilities-color.html">Categorias</a>
                        <a class="collapse-item" href="utilities-border.html">Subcategorias</a>
                        <a class="collapse-item" href="<?=base_url('index.php/bannerctrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>      


                             <!-- Nav Item - Utilities Collapse Menu -->
                                            <!--Marcas-->
                                            <li class="nav-item">
        <a class="nav-link collapsed" href="<?=base_url('index.php/marcactrl')?>" data-toggle="collapse" data-target="#collapseSix"
                    aria-expanded="true" aria-controls="collapseSix">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>Marca</span>
                </a>
                <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">                  
                        
                        <a class="collapse-item" href="<?=base_url('index.php/marcactrl')?>">Conteudo</a>

                    </div>
                </div>
            </li>   

             <!-- Nav Item - Pages Collapse Menu -->
             <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                    aria-expanded="true" aria-controls="collapsePages">
                    <i class="fas fa-user-cog mr-3"></i>
                    <span>Acesso</span>
                </a>
                <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                       
                        <a class="collapse-item" href="<?=base_url('index.php/perfilctrl/index');?>">Editar Perfil</a>
                        <a class="collapse-item" href="<?=base_url('index.php/loginctrl/logout');?>">Sair</a>            
                                          
                    </div>
                </div>
            </li>
            
        </ul>
                                     <!-- End of Sidebar -->     
                                       <!-- Main Content -->
            <div id="content">      
            
             
             
        </div>
            

          

           

    <!-- Bootstrap core JavaScript-->
    <script src="<?=base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?=base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?=base_url();?>assets/js/sb-admin-2.min.js"></script>

    <!-- Page level plugins -->
    <script src="<?=base_url();?>assets/vendor/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="<?=base_url();?>assets/js/demo/chart-area-demo.js"></script>
    <script src="<?=base_url();?>assets/js/demo/chart-pie-demo.js"></script>

