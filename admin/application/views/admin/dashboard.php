<?php $menu['active1']="dashboard";$this->load->view('admin/header',$menu); ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.min.js"></script>
    <script>
        var poolColors = function (a) {
            var pool = [];
            for(i=0;i<a;i++){
                pool.push(dynamicColors());
            }
            return pool;
        };

        var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
        };
    </script>
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row m-0 col-border-xl">
                    <div class="col-12">
                        <div class="card-body text-center">
                            <h1 class="m-0 text-uppercase">Estatísticas do Site</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row m-0 col-border-xl">
                    <div class="col-md-8 offset-md-2">
                        <div class="card-body text-center">

                            <form class="form-horizontal" id="formulario" method="post" action="<?=base_url('index.php/dashboardctrl/dashboard')?>">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text" id="">Escolha as datas:</span>
                                </div>
                                    <input type="text" name="startDate" class="form-control datepicker" placeholder="inicio" value="<?=isset($startDate)?date("d/m/Y", strtotime($startDate)):''?>" required>
                                    <input type="text" name="endDate" class="form-control datepicker" placeholder="fim"  value="<?=isset($endDate)?date("d/m/Y", strtotime($endDate)):''?>" required>
                                    <button type="submit" class="btn btn-primary btn-md" id="enviar" style="border-radius: 0 4px 4px 0;"><i class="fas fa-cog"></i> Gerar</button>

                            </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- DESTAQUES -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card pb-2">
                <div class="row m-0 col-border-xl">
                    <div class="col-md-12">
                        <h2 class="text-center text-uppercase my-3">Destaques</h2>
                    </div>
                    <div class="col-md-4">
                        <div class="card-body row" style="padding: 6px;">
                            <div class="col-md-3">
                                <div class="icon-rounded icon-rounded-success float-left mt-2 m-r-20">
                                    <i class="fas fa-users"></i>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <h6 class="text-muted m-t-10">Média de acessos por semana</h6>
                                <?php
                                $sum = 0;
                                foreach($googleAnalytics['yearWeek'] as $num) {
                                    $sum += $num[1];
                                }
                                $mediaAcessosSemana = $sum/count($googleAnalytics['yearWeek']);
                                echo '<h3 class="card-title m-b-5 counter" data-count="'.$mediaAcessosSemana.'">'.$mediaAcessosSemana.'</h3>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-body row" style="padding: 6px;">
                            <div class="col-md-3">
                                <div style="" class="icon-rounded icon-rounded-success float-left mt-2 m-r-20">
                                    <i class="fas fa-map-marker-alt"></i>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <h6 class="text-muted m-t-10">Cidade que mais acessa</h6>
                                <?php
                                $limite = 0;
                                $cidadeMaisAcessada = "";
                                foreach ($googleAnalytics['city'] as $cidade){
                                    if($cidade[0]<>"(not set)"){
                                        if(++$limite==1){
                                            $cidadeMaisAcessada = $cidade[0];
                                            break;
                                        }
                                    }
                                }
                                echo '<h3 class="card-title m-b-5">'.$cidadeMaisAcessada.'</h3>';
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card-body row" style="padding: 6px;">
                            <div class="col-md-3">
                                <div style="" class="icon-rounded icon-rounded-success float-left mt-2 m-r-20">
                                    <i class="fas fa-globe"></i>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <h6 class="text-muted m-t-10">Dispositivo que mais acessa</h6>
                                <?php
                                $limite = 0;
                                $dispositivoMaisAcessado = "";
                                foreach ($googleAnalytics['deviceCategory'] as $cidade){
                                    if(++$limite==1){
                                        $dispositivoMaisAcessado = $cidade[0];
                                        break;
                                    }
                                }
                                echo '<h3 class="card-title m-b-5 text-capitalize">'.$dispositivoMaisAcessado.'</h3>';
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->

    <!-- GERANDO ESTATISTICAS DE ACESSOS AGRUPADO POR SEMANA -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <div class="col-md-12">
                        <h5 class="text-center text-uppercase my-3">Acessos no site agrupado por semana</h5>
                    </div>
                    <div class="col-md-12">
                        <canvas id="yearWeek" height="400"></canvas>
                        <?php
                        $yearWeek_dia = "";
                        $yearWeek_qtd = "";
                        foreach ($googleAnalytics['yearWeek'] as $value){
                            $ano = substr($value[0],0,4);
                            $semana = substr($value[0],4,2);
                            $dia = getWeek($semana,$ano);
                            $yearWeek_dia .= "'".$dia['week_start']." - ".$dia['week_end']."',";
                            $yearWeek_qtd .= $value[1].",";
                        }
                        ?>

                        <script>
                            var ctx_yearWeek = document.getElementById('yearWeek').getContext('2d');
                            var yearWeek = new Chart(ctx_yearWeek, {
                                type: 'line',
                                options: {
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    scales: {
                                        yAxes: [{
                                            ticks: {
                                                beginAtZero:true
                                                //suggestedMax: ''
                                            },
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Usuarios'
                                            }
                                        }],
                                        xAxes: [{
                                            scaleLabel: {
                                                display: true,
                                                labelString: 'Semana'
                                            }
                                        }]
                                    }
                                },
                                data: {
                                    labels: [<?=$yearWeek_dia?>],
                                    datasets: [{
                                        label: "Usuários",
                                        data: [<?=$yearWeek_qtd?>],
                                        backgroundColor: "rgba(255,153,0,0.6)"
                                    }]
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->

    <!-- GERANDO ESTATISTICAS DE ACESSOS POR DISPOSITIVO E NAVEGADOR -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <!-- DISPOSITIVO -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Acessos no site por dispositivos</h5>
                            </div>
                            <div class="col-md-12">
                                <canvas id="deviceCategory" height="300"></canvas>
                                <?php
                                $deviceCategory_string = "";
                                $deviceCategory_int = "";
                                foreach ($googleAnalytics['deviceCategory'] as $value){
                                    $deviceCategory_string .= "'".$value[0]."',";
                                    $deviceCategory_int .= $value[1].",";
                                }
                                ?>
                                <script>
                                    var ctx_deviceCategory = document.getElementById('deviceCategory').getContext('2d');
                                    var deviceCategory = new Chart(ctx_deviceCategory, {
                                        type: 'pie',
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            tooltips: {
                                                callbacks: {
                                                    label: function(tooltipItem, data) {
                                                        //get the concerned dataset
                                                        var dataset = data.datasets[tooltipItem.datasetIndex];
                                                        //calculate the total of this data set
                                                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                                            return previousValue + currentValue;
                                                        });
                                                        //get the current items value
                                                        var currentValue = dataset.data[tooltipItem.index];
                                                        //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                                                        return " "+currentValue+" ("+precentage+"%)";
                                                    }
                                                }
                                            }
                                        },
                                        data: {
                                            labels: [<?=$deviceCategory_string?>],
                                            datasets: [{
                                                label: "Acessos",
                                                data: [<?=$deviceCategory_int?>],
                                                backgroundColor: poolColors(<?=count($googleAnalytics['deviceCategory'])?>)
                                            }]
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                    <!-- NAVEGADOR -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Top 5 navegadores que acessam o site</h5>
                            </div>
                            <div class="col-md-12">
                                <canvas id="browser" height="300"></canvas>
                                <?php
                                $browser_string = "";
                                $browser_int = "";
                                foreach ($googleAnalytics['browser'] as $value){
                                    $browser_string .= "'".$value[0]."',";
                                    $browser_int .= $value[1].",";
                                }
                                ?>
                                <script>
                                    var ctx_browser = document.getElementById('browser').getContext('2d');
                                    var browser = new Chart(ctx_browser, {
                                        type: 'pie',
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            tooltips: {
                                                callbacks: {
                                                    label: function(tooltipItem, data) {
                                                        //get the concerned dataset
                                                        var dataset = data.datasets[tooltipItem.datasetIndex];
                                                        //calculate the total of this data set
                                                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                                            return previousValue + currentValue;
                                                        });
                                                        //get the current items value
                                                        var currentValue = dataset.data[tooltipItem.index];
                                                        //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                                                        return " "+currentValue+" ("+precentage+"%)";
                                                    }
                                                }
                                            }
                                        },
                                        data: {
                                            labels: [<?=$browser_string?>],
                                            datasets: [{
                                                label: "Acessos",
                                                data: [<?=$browser_int?>],
                                                backgroundColor: poolColors(<?=count($googleAnalytics['browser'])?>)
                                            }]
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->

    <?php
    /*
    <!-- GERANDO ESTATISTICAS DE PERFIL DOS USUÁRIOS -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <!-- IDADE -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Faixa de idade dos usuários</h5>
                            </div>
                            <div class="col-md-12">
                                <canvas id="userAgeBracket" height="300"></canvas>
                                <?php
                                $userAgeBracket_string = "";
                                $userAgeBracket_int = "";
                                foreach ($googleAnalytics['userAgeBracket'] as $value){
                                    $userAgeBracket_string .= "'".$value[0]."',";
                                    $userAgeBracket_int .= $value[1].",";
                                }
                                ?>
                                <script>
                                    var ctx_userAgeBracket = document.getElementById('userAgeBracket').getContext('2d');
                                    var userAgeBracket = new Chart(ctx_userAgeBracket, {
                                        type: 'bar',
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            tooltips: {
                                                callbacks: {
                                                    label: function(tooltipItem, data) {
                                                        //get the concerned dataset
                                                        var dataset = data.datasets[tooltipItem.datasetIndex];
                                                        //calculate the total of this data set
                                                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                                            return previousValue + currentValue;
                                                        });
                                                        //get the current items value
                                                        var currentValue = dataset.data[tooltipItem.index];
                                                        //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                                                        return " "+currentValue+" ("+precentage+"%)";
                                                    }
                                                }
                                            }
                                        },
                                        data: {
                                            labels: [<?=$userAgeBracket_string?>],
                                            datasets: [{
                                                label: "Faixa etária",
                                                data: [<?=$userAgeBracket_int?>],
                                                backgroundColor: "rgba(255,153,0,0.6)"
                                            }]
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                    <!-- GENERO -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Gênero dos usuários</h5>
                            </div>
                            <div class="col-md-12">
                                <canvas id="userGender" height="300"></canvas>
                                <?php
                                $userGender_string = "";
                                $userGender_int = "";
                                foreach ($googleAnalytics['userGender'] as $value){
                                    $userGender_string .= "'".$value[0]."',";
                                    $userGender_int .= $value[1].",";
                                }
                                ?>
                                <script>
                                    var ctx_userGender = document.getElementById('userGender').getContext('2d');
                                    var userGender = new Chart(ctx_userGender, {
                                        type: 'pie',
                                        options: {
                                            responsive: true,
                                            maintainAspectRatio: false,
                                            tooltips: {
                                                callbacks: {
                                                    label: function(tooltipItem, data) {
                                                        //get the concerned dataset
                                                        var dataset = data.datasets[tooltipItem.datasetIndex];
                                                        //calculate the total of this data set
                                                        var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                                            return previousValue + currentValue;
                                                        });
                                                        //get the current items value
                                                        var currentValue = dataset.data[tooltipItem.index];
                                                        //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                                        var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                                                        return " "+currentValue+" ("+precentage+"%)";
                                                    }
                                                }
                                            }
                                        },
                                        data: {
                                            labels: [<?=$userGender_string?>],
                                            datasets: [{
                                                label: "Gênero",
                                                data: [<?=$userGender_int?>],
                                                backgroundColor: [
                                                    "#d7248e",
                                                    "#2126f3"
                                                ]
                                            }]
                                        }
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->
     */
    ?>
    <!-- GERANDO ESTATISTICAS GEOGRAFICAS -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <!-- BRAZIL -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Acessos das Top 10 cidades (Brasil)</h5>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive p-1">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Cidade</th>
                                            <th>Usuários</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($googleAnalytics['city'] as $value){ if($value[0]<>"(not set)"){?>
                                            <tr>
                                                <td><?=$value[0]?></td>
                                                <td><?=$value[1]?></td>
                                            </tr>
                                        <?php }} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                    <!-- FORA BRAZIL -->
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12">
                                <h5 class="text-center text-uppercase my-3">Acessos das Top 10 cidades (Fora do Brasil)</h5>
                            </div>
                            <div class="col-md-12">
                                <div class="table-responsive p-1">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Cidade</th>
                                            <th>Usuários</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        foreach ($googleAnalytics['city_sembr'] as $value){ if($value[0]<>"(not set)"){?>
                                            <tr>
                                                <td><?=$value[0]?></td>
                                                <td><?=$value[1]?></td>
                                            </tr>
                                        <?php }} ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ----- -->
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->

    <!-- GERANDO ESTATISTICAS SOURCE -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <div class="col-md-12">
                        <h5 class="text-center text-uppercase my-3">Fonte dos acessos</h5>
                    </div>
                    <div class="col-md-6 offset-md-3">
                        <canvas id="channelGrouping" height="300"></canvas>
                        <?php
                        $channelGrouping_string = "";
                        $channelGrouping_int = "";
                        foreach ($googleAnalytics['channelGrouping'] as $value){
                            $channelGrouping_string .= "'".$value[0]."',";
                            $channelGrouping_int .= $value[1].",";
                        }
                        ?>
                        <script>
                            var ctx_channelGrouping = document.getElementById('channelGrouping').getContext('2d');
                            var channelGrouping = new Chart(ctx_channelGrouping, {
                                type: 'pie',
                                options: {
                                    responsive: true,
                                    maintainAspectRatio: false,
                                    tooltips: {
                                        callbacks: {
                                            label: function(tooltipItem, data) {
                                                //get the concerned dataset
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                //calculate the total of this data set
                                                var total = dataset.data.reduce(function(previousValue, currentValue, currentIndex, array) {
                                                    return previousValue + currentValue;
                                                });
                                                //get the current items value
                                                var currentValue = dataset.data[tooltipItem.index];
                                                //calculate the precentage based on the total and current item, also this does a rough rounding to give a whole number
                                                var precentage = Math.floor(((currentValue/total) * 100)+0.5);

                                                return " "+currentValue+" ("+precentage+"%)";
                                            }
                                        }
                                    }
                                },
                                data: {
                                    labels: [<?=$channelGrouping_string?>],
                                    datasets: [{
                                        label: "Source",
                                        data: [<?=$channelGrouping_int?>],
                                        backgroundColor: poolColors(<?=count($googleAnalytics['channelGrouping'])?>)
                                    }]
                                }
                            });
                        </script>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->

    <!-- GERANDO ESTATISTICAS PAGINAS ACESSADAS -->
    <div class="row px-2">
        <div class="col-12">
            <div class="card">
                <div class="row my-3 col-border-xl">
                    <div class="col-md-12">
                        <h5 class="text-center text-uppercase my-3">Acessos pelas páginas</h5>
                    </div>
                    <div class="col-md-8 offset-md-2">
                        <div class="table-responsive p-1">
                            <table class="table table-striped table-sm">
                                <thead>
                                <tr>
                                    <th>Página</th>
                                    <th>Acessos</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($googleAnalytics['pagePath'] as $value){ ?>
                                    <tr>
                                        <td><?=$value[0]?></td>
                                        <td><?=$value[1]?></td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ---------------------- -->
</section>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/locales/bootstrap-datepicker.pt-BR.min.js"></script>

<script>
    $(document).ready(function() {
        /******* CALENDARIO INPUT *******/
        $('.datepicker').datepicker({
            language: 'pt-BR'
        }).on('changeDate', function(e){
            $(this).datepicker('hide');
        });
        /******* *******/

        /******* VALIDACAO FORMULARIO - SUBMIT *******/
        $('#formulario').validate({
            rules: {
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Gerando');
                return true;
            },
        });
        /******* *******/

    });
</script>
