<?php $menu['active1']="pecascategoria";$menu['active2']="pecasctrl";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
<section class="page-content container-fluid" xmlns:>
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase">Ordenar</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form autocomplete="off" class="form-horizontal" id="formulario" method="post" action="<?=base_url('index.php/pecasctrl/saveorderproduto')?>">
                        <div class="card-header">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/pecasctrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- CAMPOS -->
                        <div class="card-body">
                            <div class="row" id='sortme'>
                                <?php foreach ($pecas as $value){ ?>
                                    <div class="col-md-3" id='item_<?=isset($value->id)?$value->id:'';?>'>
                                        <div class="card card-border-success" style="cursor: move;">
                                            <div class="card-header" style="padding:3px 20px;min-height: 31px;">Arrastar item
                                                <ul class="actions top-right" style="right: 11px;top: 2px;">
                                                    <li><a href="javascript:void(0)" style="cursor: move;"><i class="fas fa-exchange-alt"></i></a></li>
                                                </ul>
                                            </div>

                                            <div class="card-body block-el">
                                                <div class="" id="imagem-principal" style="background-image: url('<?=isset($value->imagem_principal)?$value->imagem_principal:''?>');background-size: cover;background-position: center;background-repeat: no-repeat;width: 100%;height: 90px;border: 1px solid #efefef;"></div>
                                                <div style="padding-top:7px;font-size: 11px;line-height: 13px;" class="control-label text-center"><?=isset($value->titulo)?$value->titulo:''?></div>
                                            </div>
                                        </div>
                                    </div>

                                <?php } ?>
                            </div>
                        </div>
                        <!-- ---------------- -->
                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/pecasctrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script>
    $(document).ready(function() {
        /******* ENVIANDO A ORDENAÇÃO VIA AJAX *******/
        $( "#sortme" ).sortable({
            update : function () {
                serial = $('#sortme').sortable('serialize');
                $.ajax({
                    url: "<?=base_url('index.php/pecasctrl/ajaxorder')?>",
                    type: "post",
                    data: serial,
                    success: function(data){
                        console.log(data);
                    },
                    error: function(data){
                        console.log("theres an error with AJAX");
                    }
                });
            }
        });
        $( "#sortme" ).disableSelection();
        /******* *******/
    });
</script>
