<?php $menu['active1']="banner";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase"><?=isset($value[0]->id)?'Editar':'Criar'?> Banner</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form autocomplete="off" class="form-horizontal" id="formulario" method="post" action="<?=isset($value[0]->id)?base_url('index.php/bannerctrl/updatebanner'):base_url('index.php/bannerctrl/insertbanner');?>">
                        <input type="hidden" name="id" class="form-control" value="<?=isset($value[0]->id)?$value[0]->id:''?>">
                        <div class="tab-panel">
                            <ul class="nav nav-tabs info-tabs justify-content-center">
                                <li class="nav-item" role="presentation"><a href="#tab" class="nav-link active show" data-toggle="tab" aria-expanded="true"><i class="fas fa-align-left"></i> Conteúdo</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fadeIn active" id="tab">
                                    <!-- CAMPOS PORTUGUÊS -->
                                    <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Ativar / Desativar</label>
                                                <div class="col-md-5">
                                                    <input class="tgl tgl-light tgl-success" id="cb8" name="ativo" type="checkbox" <?=(!isset($value[0]->ativo)||$value[0]->ativo==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb8"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Link</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="link" class="form-control" value="<?=isset($value[0]->link)?$value[0]->link:''?>" placeholder="Opcional">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4 align-self-center">Imagem<br><small>Indicado - 1600x625 pixels</small></label>
                                                <div class="col-md-5">
                                                    <div class="imagem display: <?=isset($value[0]->imagem_principal)&&$value[0]->imagem_principal?'no-bg':''?>" id="imagem-principal" style="background-image: url('<?=isset($value[0]->imagem_principal)?$value[0]->imagem_principal:''?>')"></div>
                                                    <div class="text-center"><a class="img-remove" href="javascript:undefined;" style="display: <?=isset($value[0]->imagem_principal)&&$value[0]->imagem_principal?'block':'none'?>">&gt;&gt; Excluir imagem principal &lt;&lt;</a></div>
                                                    <input type="hidden" name="imagem_principal" id="imagem" value="<?=isset($value[0]->imagem_principal)?$value[0]->imagem_principal:''?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ---------------- -->
                                </div>
                            </div>
                        </div>
                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <?php if(isset($value[0]->id)) { ?>
                                                    <a role="button" class="deletar btn btn-danger btn-md">
                                                        <i class="far fa-trash-alt" style="color: inherit !important;"></i> Deletar
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/bannerctrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php if(isset($value[0]->id)) { ?>
    <form method="post" class="form-horizontal d-inline"  action="<?=base_url('index.php/bannerctrl/deleteBanner')?>" id="formulario_deletar">
        <input type="hidden" name="id" value="<?=$value[0]->id?>">
    </form>
<?php } ?>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script src="<?=base_url();?>assets/ckfinder/ckfinder.js"></script>
<script src="//cdn.ckeditor.com/ckeditor5/10.1.0/classic/translations/pt-br.js"></script>
<script>
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
        var to   = "aaaaaeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    $(document).ready(function() {

        /******* CKFINDER - Imagem Principal *******/
        var button1 = document.getElementById( 'imagem-principal' );
        button1.onclick = function() {
            selectFileWithCKFinder( 'imagem-principal' );
        };
        function selectFileWithCKFinder( elementId ) {
            CKFinder.popup( {
                chooseFiles: true,
                width: 1000,
                height: 600,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var output = document.getElementById( "imagem" );
                        output.value = file.getUrl();
                        $('#imagem-principal').css('background-image', 'url("' + file.getUrl() + '")').addClass("no-bg");
                        $(".img-remove").show();
                    } );

                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        var output = document.getElementById( "imagem" );
                        output.value = evt.data.resizedUrl;
                        $('#imagem-principal').css('background-image', 'url("' + evt.data.resizedUrl + '")').addClass("no-bg");
                        $(".img-remove").show();
                    } );
                }
            } );
        }
        $(".img-remove").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("#imagem").val('');
            $('#imagem-principal').css('background-image', 'url("")').removeClass("no-bg");
            $(".img-remove").hide();
        });
        /******* *******/

        /******* BOTAO DELETAR *******/
        $(".deletar").click(function(e) {
            e.preventDefault();
            var form = $("#formulario_deletar");
            swal({
                title: "Tem certeza que deseja deletar?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                cancelButtonText: "Não, quero cancelar",
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        form.submit();
                        setTimeout(function() {
                            resolve();
                        }, 2000);
                    });
                }
            })
        });
        /******* *******/

        /******* VALIDACAO FORMULARIO - SUBMIT *******/
        $('#formulario').validate({
            rules: {
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                return true;
            },
        });
        /******* *******/
    });
</script>
