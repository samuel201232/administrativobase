<?php $menu['active1']="banner";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
<section class="page-content container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row m-0 col-border-xl">
                    <div class="col-12">
                        <div class="card-body text-center">
                            <h1 class="m-0 text-uppercase">Banner</h1>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="text-center card-header">
                    <a role="button" class="btn btn-primary btn-block mb-0 max-w-300" href="<?=base_url('index.php/bannerctrl/createbanner')?>">
                        <i class="fas fa-plus-circle"></i> Adicionar Novo
                    </a>
                </div>

                <div class="card-body">
                    <div class="text-center" id="preloader">
                        <div class="preloader pl-xxl pls-primary">
                            <svg class="pl-circular" viewBox="25 25 50 50">
                                <circle class="plc-path" cx="50" cy="50" r="20"></circle>
                            </svg>
                        </div>
                    </div>
                    <table id="bs4" class="table table-striped table-bordered d-none" style="width:100%">
                        <thead>
                        <tr>
                            <th style="max-width: 40px;">ID <i class="fas fa-question-circle"  data-toggle="tooltip" data-placement="top" title="" data-original-title="O ID é universal, ele é o código de referência do cadastro"></i></th>
                            <th style="max-width: 50px;">Ações</th>
                            <th>Imagem</th>
                            <th>Link</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        if($banner)
                            foreach ($banner as $value){ ?>
                                <tr>
                                    <td>#<?=$value->id?></td>
                                    <td class="">
                                        <a class="editar" href="<?=base_url('index.php/bannerctrl/selectBanner?id=').$value->id?>">
                                            <i class="far fa-edit" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar"></i>
                                        </a>
                                        &nbsp;&nbsp;
                                        <form method="post" class="form-horizontal d-inline"  action="<?=base_url('index.php/bannerctrl/deleteBanner')?>">
                                            <a class="deletar">
                                                <i class="far fa-trash-alt" data-toggle="tooltip" data-placement="top" title="" data-original-title="Deletar"></i>
                                            </a>
                                            <input type="hidden" name="id" value="<?=$value->id?>">
                                        </form>
                                    </td>
                                    <td><?=$value->imagem_principal?'<a data-fancybox href="'.$value->imagem_principal.'"><img src="'.$value->imagem_principal.'" class="img-fluid" style="max-height:65px;"></a>':''?></td>
                                    <td><?=$value->link?'<a target="_blank" href="'.$value->link.'">'.$value->link.'</a>':''?></td>
                                    <td>
                                        <form method="post" class="form-horizontal d-inline"  action="">

                                            <input class="tgl tgl-light tgl-success" id="cb<?=$value->id?>" name="ativo" type="checkbox" <?=(isset($value->ativo)&&$value->ativo==1)?'checked':''?>>
                                            <label class="tgl-btn btn-ativo mb-0" for="cb<?=$value->id?>"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                            <input type="hidden" name="id" value="<?=$value->id?>">
                                        </form>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</section>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<style>
    a[data-fancybox]{cursor: zoom-in;}
</style>
<script src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>

<script>
    $(document).ready(function() {
        $('#bs4').DataTable({
            "order": [[ 2, "asc" ]],
            "language": {
                "url": "<?=base_url();?>assets/vendor/datatables.net/js/pt-br.json"
            },
            "initComplete": function(settings, json) {
                $(this).removeClass("d-none");
                $("#preloader").addClass("d-none");
            }
        }).on('click', '.deletar', function(e){
            e.preventDefault();
            var form = $(this).parents('form');
            swal({
                title: "Tem certeza que deseja deletar?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                cancelButtonText: "Não, quero cancelar",
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        form.submit();
                        setTimeout(function() {
                            resolve();
                        }, 2000);
                    });
                }
            })
        }).on('click', '.btn-ativo', function(e){

            var form = $(this).parents('form');

            checkbox = $(form).find('input[type=checkbox]');
            id = $(form).find('input[type=hidden]').val();
            if(checkbox.is(':checked')){
                valors = "ativo=off&id="+id;
            }else{
                valors = "ativo=on&id="+id;
            }

            console.log(valors);
            $.ajax({
                url: '<?=base_url('index.php/bannerctrl/statusBanner')?>',
                type: 'POST',
                data: valors,
                success: function(resposta){
                    console.log(resposta);
                },
                error : function(jqXHR, textStatus, errorThrown){
                    console.log("jqXHR: "+jqXHR.status);
                    console.log("textStatus: "+textStatus);
                    console.log("errorThrown: "+errorThrown);
                }
            });
            return true;
        }).on('click', '.btn-matricula_aberta', function(e){

            var form = $(this).parents('form');

            checkbox = $(form).find('input[type=checkbox]');
            id = $(form).find('input[type=hidden]').val();
            if(checkbox.is(':checked')){
                valors = "matricula_aberta=off&id="+id;
            }else{
                valors = "matricula_aberta=on&id="+id;
            }

            console.log(valors);
            $.ajax({
                url: '<?=base_url('index.php/bannerctrl/matriculaBanner')?>',
                type: 'POST',
                data: valors,
                success: function(resposta){
                    console.log(resposta);
                },
                error : function(jqXHR, textStatus, errorThrown){
                    console.log("jqXHR: "+jqXHR.status);
                    console.log("textStatus: "+textStatus);
                    console.log("errorThrown: "+errorThrown);
                }
            });
            return true;
        });

    });
</script>
