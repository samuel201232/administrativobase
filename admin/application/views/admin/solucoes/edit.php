<?php $menu['active1']="solucoescategoria";$menu['active2']="Solucoesctrl";$this->load->view('admin/header',$menu); ?>
<!--START PAGE CONTENT -->
    <section class="page-content container-fluid">
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="row m-0 col-border-xl">
                        <div class="col-12">
                            <div class="card-body text-center">
                                <h1 class="m-0 text-uppercase"><?=isset($value[0]->id)?'Editar':'Criar'?> Solucoes</h1>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card">
                    <form autocomplete="off" class="form-horizontal" id="formulario" method="post" enctype="multipart/form-data" action="<?=base_url('index.php/Solucoesctrl/insertOrUpdate')?>">
                        <input type="hidden" name="id" class="form-control" value="<?=isset($value[0]->id)?$value[0]->id:''?>">
                        <div class="tab-panel">
                            <ul class="nav nav-tabs info-tabs justify-content-center">
                                <li class="nav-item" role="presentation"><a href="#tab" class="nav-link active show" data-toggle="tab" aria-expanded="true"><i class="fas fa-align-left"></i> Conteúdo</a></li>
                                <li class="nav-item" role="presentation"><a href="#tab_img" class="nav-link" data-toggle="tab" aria-expanded="true"><i class="far fa-images"></i> Imagens</a></li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane fadeIn active" id="tab">
                                    <!-- CAMPOS PORTUGUÊS -->
                                    <div class="card-body">
                                        <div class="form-body">

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Ativar / Desativar</label>
                                                <div class="col-md-5">
                                                    <input class="tgl tgl-light tgl-success" id="cb8" name="ativo" type="checkbox" <?=(!isset($value[0]->ativo)||$value[0]->ativo==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb8"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
											</div>

											<div class="form-group row">
                                                <label class="control-label text-right col-md-4">Destaque / Não é Destaque</label>
                                                <div class="col-md-5">
													<input class="tgl tgl-light tgl-success" id="cb_8" name="destaque" type="checkbox" <?=(isset($value[0]->destaque)&&$value[0]->destaque==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb_8"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
											</div>

											<div class="form-group row">
                                                <label class="control-label text-right col-md-4">Usado</label>
                                                <div class="col-md-5">
													<input class="tgl tgl-light tgl-success" id="cb_8b" name="usado" type="checkbox" <?=(isset($value[0]->usado)&&$value[0]->usado==1)?'checked':''?> >
                                                    <label class="tgl-btn" for="cb_8b"  data-toggle="tooltip" data-html="true" data-placement="top" title="<spam style='color:#2fbfa0'>Verde = Ativado</spam><br><spam style='color:#d2deec'>Cinza = Desativado</spam>"></label>
                                                </div>
											</div>

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Categoria</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="id_categoria" id="id_categoria">
                                                        <option value="">-- Relacione a categoria --</option>
                                                        <?php foreach ($categorias as $categoria){
                                                            $select = ($categoria->id==$value[0]->id_categoria)?'selected':'';
                                                            echo '<option value="'.$categoria->id.'" '.$select.'>'.$categoria->titulo.'</option>';
                                                        } ?>
                                                    </select>
                                                </div>
											</div>

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Sub Categoria</label>
                                                <div class="col-md-5">
                                                    <select class="form-control" name="id_subcategoria" id="id_subcategoria">
                                                        <option value="">-- Relacione a sub categoria --</option>  
                                                        <?php foreach ($subcategorias as $subcategoria){
                                                            $select = ($subcategoria->id==$value[0]->id_subcategoria)?'selected':'';
                                                            echo '<option value="'.$subcategoria->id.'" '.$select.'>'.$subcategoria->titulo.'</option>';
                                                        } ?>                                                      
                                                    </select>
                                                </div>
											</div>
											
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Título</label>
                                                <div class="col-md-5">
                                                    <input type="text" name="titulo" class="form-control" value="<?=isset($value[0]->titulo)?$value[0]->titulo:''?>" placeholder="Obrigatório" required>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4">Resumo</label>
                                                <div class="col-md-5">
                                                    <textarea name="resumo" class="form-control" rows="5" placeholder="Opcional"><?=isset($value[0]->resumo)?$value[0]->resumo:''?></textarea>
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <div class="col-md-10 offset-md-1">
                                                    <hr>
                                                    <div class="text-center"><h5>Descrição</h5></div>
                                                    <textarea name="descricao" class="form-control" id="descricao" rows="50"><?=isset($value[0]->descricao)?$value[0]->descricao:''?></textarea>
                                                </div>
                                            </div>

											<div class="form-group row">
                                                <div class="col-md-10 offset-md-1">
                                                    <hr>
                                                    <div class="text-center"><h5>Visão Geral</h5></div>
                                                    <textarea name="visao_geral" class="form-control" id="visao_geral" rows="50"><?=isset($value[0]->visao_geral)?$value[0]->visao_geral:''?></textarea>
                                                </div>
                                            </div>

											<div class="form-group row">
                                                <div class="col-md-10 offset-md-1">
                                                    <hr>
                                                    <div class="text-center"><h5>Especificações Técnicas</h5></div>
                                                    <textarea name="especificacoes_tecnicas" class="form-control" id="especificacoes_tecnicas" rows="50"><?=isset($value[0]->especificacoes_tecnicas)?$value[0]->especificacoes_tecnicas:''?></textarea>
                                                </div>
                                            </div>

											<div class="form-group row">
                                                <div class="col-md-10 offset-md-1">
                                                    <hr>
                                                    <div class="text-center"><h5>Acessórios</h5></div>
                                                    <textarea name="acessorios" class="form-control" id="acessorios" rows="50"><?=isset($value[0]->acessorios)?$value[0]->acessorios:''?></textarea>
                                                </div>
                                            </div>

											<div class="form-group row">
                                                <div class="col-md-10 offset-md-1">
                                                    <hr>
                                                    <div class="text-center"><h5>Garantia</h5></div>
                                                    <textarea name="garantia" class="form-control" id="garantia" rows="50"><?=isset($value[0]->garantia)?$value[0]->garantia:''?></textarea>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <!-- ---------------- -->
                                </div>
                                <div class="tab-pane fadeIn" id="tab_img">
                                    <!-- CAMPOS IMAGENS -->
                                    <!-- ===== IMAGEM PRINCIPAL ===== -->
                                    <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4 align-self-center">Imagem
                                                    principal </br> 566 x 566 px </br> 72dpi</label>
                                                <div class="col-md-5">
                                                    <input type="file" accept="image/x-png,image/gif,image/jpeg" name="imagem_principal" class="form-control"
                                                        id="imagem_principal" style="display: none;"></br>
                                                    </br>
                                                    <div class="imagem_container_principal" style="width:300px;height:300px;border: 1px dashed; color:#ff8000;cursor:pointer" id="imagem_container_principal"
                                                        style="text-align: center">
                                                        <?php if(isset($value[0]->imagem_principal) && !empty($value[0]->imagem_principal)) { ?>
                                                            <img src="<?=base_url('public/'.$value[0]->imagem_principal)?>"
                                                                class="single-preview" id="img_principal_preview">        
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>



                                    <!-- ===== ARQUIVO ===== -->
                                    <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label text-right col-md-4 align-self-center">Arquivo</label>
                                                <div class="col-md-5">
                                                    <input type="file" name="arquivo" class="form-control"
                                                        id="arquivo" style="display: none;"></br>
                                                    </br>
                                                    <div class="arquivo_container" style="width:300px;height:300px;border: 1px dashed; color:#ff8000;cursor:pointer" id="arquivo_container"
                                                        style="text-align: center">
                                                        <?php if(isset($value[0]->arquivo) && !empty($value[0]->arquivo)) { ?>
                                                            <?php if(in_array(pathinfo($value[0]->arquivo)['extension'],['jpg','jpeg','png'])) { ?>
                                                                <img src="<?=base_url('public/files/').$value[0]->arquivo?>"
                                                                    class="single-preview">        
                                                            <?php }else{ ?>
                                                                <img src="<?=base_url('public/admin/images/file-icon.png')?>"
                                                                    class="single-preview">  
                                                            <?php } ?>      
                                                        <?php } ?>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                     <!-- ===== GALERIA ===== -->
                                     <div class="card-body">
                                        <div class="form-body">
                                            <div class="form-group row text-center">
                                               <h5 style="margin:0 auto;">Galeria de Imagens<br><small style="font-size: 11px;">Indicado - 800x600 pixels</small></h5>
                                                <div id="input_img_dropzone">
                                                    <?php
                                                    if(isset($galeria)&&is_array($galeria)){
                                                    foreach ($galeria as $imagens){ ?>
                                                        <input name="imagem[]" value="<?=$imagens->imagem?>" type="hidden">
                                                    <?php }} ?>
                                                </div>
                                                <div class="col-md-10 offset-md-1 dropzone">
                                                    <div id="dropzone">
                                                        <?php
                                                        if(isset($galeria)&&is_array($galeria)){
                                                            foreach ($galeria as $imagens){
                                                                error_reporting(0);
                                                                $headers = get_headers(base_url().'public/img/'.$imagens->imagem, true);
                                                                $size = formatBytes($headers['Content-Length']); ?>
                                                                <div class="dz-preview dz-processing dz-image-preview dz-success dz-complete">
                                                                    <div class="dz-image"><img data-dz-thumbnail="<?=base_url().'public/img/'.$imagens->imagem?>" alt="<?=$imagens->imagem?>" src="<?=base_url().'public/img/'.$imagens->imagem?>"></div>
                                                                    <div class="dz-details">
                                                                        <div class="dz-size"><span data-dz-size=""><?=$size?></span></div>
                                                                        <div class="dz-filename"><span data-dz-name=""><?=$imagens->imagem?></span></div>
                                                                    </div>
                                                                    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress="" style="width: 100%;"></span></div>
                                                                    <div class="dz-error-message"><span data-dz-errormessage=""></span></div>
                                                                    <textarea placeholder="Legenda" name="legenda[]" class="form-control legenda"><?=$imagens->legenda?></textarea>
                                                                    <a class="dz-remove custom-remove" data-dz-remove>&gt;&gt; Excluir &lt;&lt;</a>
                                                                </div>
                                                        <?php }} ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- --------------- -->
                                </div>
                            </div>
                        </div>

                        
                        <div class="card-footer bg-light">
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col text-center">
                                                <button type="submit" class="btn btn-primary btn-md" id="enviar"><i class="fas fa-save"></i> Salvar</button>
                                                &nbsp;&nbsp;&nbsp;
                                                <?php if(isset($value[0]->id)) { ?>
                                                    <a role="button" class="deletar btn btn-danger btn-md">
                                                        <i class="far fa-trash-alt" style="color: inherit !important;"></i> Deletar
                                                    </a>
                                                    &nbsp;&nbsp;&nbsp;
                                                <?php } ?>
                                                <a role="button" class="btn btn-info btn-outline btn-md" href="<?=base_url('index.php/solucoesctrl');?>"><i class="fas fa-reply"></i> Voltar</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php if(isset($value[0]->id)) { ?>
    <form method="post" class="form-horizontal d-inline"  action="<?=base_url('index.php/Solucoesctrl/delete')?>" id="formulario_deletar">
        <input type="hidden" name="id" value="<?=$value[0]->id?>">
    </form>
<?php } ?>
<!--END PAGE CONTENT -->
<?php $this->load->view('admin/footer'); ?>
<style>
    .single-preview {
        object-position: center;
        object-fit: contain;
        height: inherit;
        width: inherit;
    }

</style>
<link rel="stylesheet" href="<?=base_url();?>assets/dropzone/min/basic.min.css" />
<link rel="stylesheet" href="<?=base_url();?>assets/dropzone/min/dropzone.min.css" />
<script src="<?=base_url();?>assets/dropzone/min/dropzone.min.js"></script>
<script src="<?=base_url();?>assets/dropzone/min/dropzone-amd-module.min.js"></script>
<script src="//cdn.ckeditor.com/4.10.0/full/ckeditor.js"></script>
<script src="<?=base_url();?>assets/ckfinder/ckfinder.js"></script>
<script src="<?=base_url();?>assets/vendor/select2/select2.min.js"></script>
<script src="//cdn.ckeditor.com/ckeditor5/10.1.0/classic/translations/pt-br.js"></script>
<script>
    function string_to_slug(str) {
        str = str.replace(/^\s+|\s+$/g, ''); // trim
        str = str.toLowerCase();

        // remove accents, swap ñ for n, etc
        var from = "àáäâãèéëêìíïîòóöôõùúüûñç·/_,:;";
        var to   = "aaaaaeeeeiiiiooooouuuunc------";
        for (var i=0, l=from.length ; i<l ; i++) {
            str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
        }

        str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
            .replace(/\s+/g, '-') // collapse whitespace and replace by -
            .replace(/-+/g, '-'); // collapse dashes

        return str;
    }

    $(document).ready(function() {

        <?php if(!isset($value[0]->id)) { ?>
            $("#id_subcategoria").empty().append('<option selected="selected" value="-1">Aguardando seleção de categoria...</option>');
        <?php } ?>

        $("#id_categoria").on("change", function(e) {
            e.preventDefault();
            let categoria = $("#id_categoria").val();
            $.ajax({
                    type:"post",
                    url: "<?=base_url();?>index.php/Solucoesctrl/getSubcategoriaByCategoria",
                    data:{categoria:categoria},
                    dataType: 'json',
                    cache: 'false',
                    success:function(resposta) {
                        console.log(resposta.value);
                        $("#id_subcategoria").empty().append('<option selected="selected" value="-1">Obrigatório</option>');
                        
                        $.each(resposta.value, function (key, value) {
                            if(value.id_categoria == categoria) {
                                $("#id_subcategoria").append('<option value="'+value.id+'">'+value.titulo+'</option>');
                            }
                        });
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(xhr.status + " - " + thrownError);
                    }
                }
            );
        });

        /******* CKEDITOR *******/
        CKEDITOR.replace('descricao', {
            filebrowserBrowseUrl: '<?=base_url();?>assets/ckfinder/ckfinder.html',
            filebrowserUploadUrl: '<?=base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });
        /*******  *******/

		/******* CKEDITOR *******/
        CKEDITOR.replace('visao_geral', {
            filebrowserBrowseUrl: '<?=base_url();?>assets/ckfinder/ckfinder.html',
            filebrowserUploadUrl: '<?=base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });
        /*******  *******/

		/******* CKEDITOR *******/
        CKEDITOR.replace('especificacoes_tecnicas', {
            filebrowserBrowseUrl: '<?=base_url();?>assets/ckfinder/ckfinder.html',
            filebrowserUploadUrl: '<?=base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });
        /*******  *******/

		/******* CKEDITOR *******/
        CKEDITOR.replace('acessorios', {
            filebrowserBrowseUrl: '<?=base_url();?>assets/ckfinder/ckfinder.html',
            filebrowserUploadUrl: '<?=base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });
        /*******  *******/

		/******* CKEDITOR *******/
        CKEDITOR.replace('garantia', {
            filebrowserBrowseUrl: '<?=base_url();?>assets/ckfinder/ckfinder.html',
            filebrowserUploadUrl: '<?=base_url();?>assets/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
            filebrowserWindowWidth: '1000',
            filebrowserWindowHeight: '700'
        });
        /*******  *******/

        /*******Imagem Principal *******/

        $('#imagem_container_principal').click(function() {
            $("input[id='imagem_principal']").click();
        });
        
        const imagem_principal = document.getElementById("imagem_principal");

        imagem_principal.addEventListener("change", function () {

            const file = this.files[0];

            if (file) {

                var imageTypes = ['jpg', 'jpeg', 'png'];  //acceptable file types
                var extension = file.name.split('.').pop().toLowerCase();  //file extension from input file
                var isImage = imageTypes.indexOf(extension) > -1;  //is extension in acceptable types

                const reader = new FileReader();

                reader.addEventListener("load", function () {
                    console.log(this);
                    if ( $('#imagem_container_principal').children().length == 0 ) {
                        $('#imagem_container_principal').prepend('<img id="img_principal_preview" class="single-preview" src="" />')
                    }
                    
                    if(isImage){
                        document.querySelector("#img_principal_preview").setAttribute("src", this.result);
                    }
                    else{
                        document.querySelector("#img_principal_preview").setAttribute("src", '<?= base_url('public/admin/images/file-icon.png') ?>');
                    }

                });

                reader.readAsDataURL(file);
            }
        });
        /******* *******/

        /******* ARQUIVO *******/

        $('#arquivo_container').click(function() {
            $("input[id='arquivo']").click();
        });
        
        const arquivo = document.getElementById("arquivo");

        arquivo.addEventListener("change", function () {

            const file = this.files[0];

            if (file) {

                var imageTypes = ['jpg', 'jpeg', 'png'];  //img file types
                var extension = file.name.split('.').pop().toLowerCase();  //file extension from input file
                var isImage = imageTypes.indexOf(extension) > -1;  //is extension in acceptable types

                const reader = new FileReader();

                reader.addEventListener("load", function () {
                    console.log(this);
                    if ( $('#arquivo_container').children().length == 0 ) {
                        $('#arquivo_container').prepend('<img id="arquivo_preview" class="single-preview" src="" />')
                    }
                    
                    if(isImage){
                        document.querySelector("#arquivo_preview").setAttribute("src", this.result);
                    }
                    else{
                        document.querySelector("#arquivo_preview").setAttribute("src", '<?= base_url('public/admin/images/file-icon.png') ?>');
                    }

                });

                reader.readAsDataURL(file);
            }
        });
        /******* *******/

        /******* CKFINDER - Imagem topo *******/
        var button2 = document.getElementById( 'imagem-topo' );
        button2.onclick = function() {
            selectFileWithCKFinderTopo( 'imagem-topo' );
        };
        function selectFileWithCKFinderTopo( elementId ) {
            CKFinder.popup( {
                chooseFiles: true,
                width: 1000,
                height: 600,
                onInit: function( finder ) {
                    finder.on( 'files:choose', function( evt ) {
                        var file = evt.data.files.first();
                        var output = document.getElementById( "imagem_topo" );
                        output.value = file.getUrl();
                        $('#imagem-topo').css('background-image', 'url("' + file.getUrl() + '")').addClass("no-bg");
                        $(".img-remove-topo").show();
                    } );

                    finder.on( 'file:choose:resizedImage', function( evt ) {
                        var output = document.getElementById( "imagem_topo" );
                        output.value = evt.data.resizedUrl;
                        $('#imagem-topo').css('background-image', 'url("' + evt.data.resizedUrl + '")').addClass("no-bg");
                        $(".img-remove-topo").show();
                    } );
                }
            } );
        }
        $(".img-remove-topo").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            $("#imagem_topo").val('');
            $('#imagem-topo').css('background-image', 'url("")').removeClass("no-bg");
            $(".img-remove-topo").hide();
        });
        /******* *******/

        /******* DROPZONE - Galeria de Imagens *******/
        $("#dropzone").dropzone({
            url: "<?=base_url('index.php/dropzonectrl')?>",
            addRemoveLinks: true,
            dictRemoveFile: ">> Excluir <<",
            dictCancelUpload: ">> Cancelar <<",
            dictCancelUploadConfirmation: "Deseja realmente cancelar o upload?",
            previewTemplate: '<div class="dz-preview dz-file-preview">\n' +
            '    <div class="dz-image"><img data-dz-thumbnail /></div>\n' +
            '    <div class="dz-details">\n' +
            '        <div class="dz-size"><span data-dz-size></span></div>\n' +
            '        <div class="dz-filename"><span data-dz-name></span></div>\n' +
            '    </div>\n' +
            '    <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>\n' +
            '    <div class="dz-error-message"><span data-dz-errormessage></span></div>\n' +
            '    <textarea placeholder="Legenda" name="legenda[]" class="form-control legenda"></textarea>\n' +
            '</div>',
            renameFile: function (file) {
                novo_nome = new Date().getTime() + '_' + string_to_slug(file.name)+ '.' +file.name.split('.').pop();
                return novo_nome;
            },
            init: function() {
                this.on("success", function(file, response) {
                    var obj = jQuery.parseJSON(response);
                    file.previewElement.id = obj[0];
                    console.log(obj[0]);
                    $('<input>').attr({
                        type: 'hidden',
                        name: 'imagem[]',
                        value: obj[0]
                    }).appendTo('#input_img_dropzone');
                });
                this.on("removedfile", function (file)
                {
                    var name = file.previewElement.id;
                    $("input[value='"+name+"']").remove();
                    $.post("<?=base_url('index.php/dropzonectrl/delete')?>?id=" + name);
                });
            }
        });

        $(".custom-remove").on("click", function (e) {
            e.preventDefault();
            e.stopPropagation();
            var name = $(this).parent().find(".dz-filename > span").text();
            var element = $(this).closest(".dz-preview");
            console.log(element);
            element.remove();
            console.log(name);
            $("input[value='"+name+"']").remove();
            $.post("<?=base_url('index.php/dropzonectrl/delete')?>?id=" + name);
        });
        /******* *******/

        /******* BOTAO DELETAR *******/
        $(".deletar").click(function(e) {
            e.preventDefault();
            var form = $("#formulario_deletar");
            swal({
                title: "Tem certeza que deseja deletar?",
                text: "",
                type: "warning",
                showCancelButton: true,
                showLoaderOnConfirm: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Sim, pode deletar",
                cancelButtonText: "Não, quero cancelar",
                preConfirm: function() {
                    return new Promise(function(resolve, reject) {
                        form.submit();
                        setTimeout(function() {
                            resolve();
                        }, 2000);
                    });
                }
            })
        });
        /******* *******/

        /******* VALIDACAO FORMULARIO - SUBMIT *******/
        $('#formulario').validate({
            rules: {
            },
            // Define as mensagens de erro para cada regra
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                return true;
            },
        });
        /******* *******/
    });
</script>
