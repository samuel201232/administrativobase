</div>
</div>
</div>
<!-- END CONTENT WRAPPER -->
<!-- ================== GLOBAL VENDOR SCRIPTS ==================-->
<script src="<?=base_url();?>assets/vendor/modernizr/modernizr.custom.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery/dist/jquery.min.js"></script>
<script src="<?=base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?=base_url();?>assets/vendor/js-storage/js.storage.js"></script>
<script src="<?=base_url();?>assets/vendor/js-cookie/src/js.cookie.js"></script>
<script src="<?=base_url();?>assets/vendor/pace/pace.js"></script>
<script src="<?=base_url();?>assets/vendor/metismenu/dist/metisMenu.js"></script>
<script src="<?=base_url();?>assets/vendor/switchery-npm/index.js"></script>
<script src="<?=base_url();?>assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
<!-- ================== PAGE LEVEL VENDOR SCRIPTS ==================-->
<script src="<?=base_url();?>assets/vendor/countup.js/dist/countUp.min.js"></script>
<script src="<?=base_url();?>assets/vendor/chart.js/dist/Chart.bundle.min.js"></script>
<script src="<?=base_url();?>assets/vendor/flot/jquery.flot.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery.flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
<script src="<?=base_url();?>assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="<?=base_url();?>assets/vendor/flot/jquery.flot.time.js"></script>
<script src="<?=base_url();?>assets/vendor/flot.curvedlines/curvedLines.js"></script>
<script src="<?=base_url();?>assets/vendor/datatables.net/js/jquery.dataTables.js"></script>
<script src="<?=base_url();?>assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
<!-- ================== GLOBAL APP SCRIPTS ==================-->
<script src="<?=base_url();?>assets/js/global/app.js"></script>
<!-- ================== PAGE LEVEL SCRIPTS ==================-->
<script src="<?=base_url();?>assets/js/components/countUp-init.js"></script>
<script src="<?=base_url();?>assets/js/cards/counter-group.js"></script>
<script src="<?=base_url();?>assets/js/cards/recent-transactions.js"></script>
<script src="<?=base_url();?>assets/js/cards/monthly-budget.js"></script>
<script src="<?=base_url();?>assets/js/cards/users-chart.js"></script>
<script src="<?=base_url();?>assets/js/cards/bounce-rate-chart.js"></script>
<script src="<?=base_url();?>assets/js/cards/session-duration-chart.js"></script>
<script src="<?=base_url();?>assets/vendor/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery.validate.min.js"></script>
<script src="<?=base_url();?>assets/vendor/jquery-validation-messages.js"></script>
<script src="<?=base_url();?>assets/vendor/formatter.min.js"></script>

<script>
    $(document).ready(function() {
        <?php
        if (isset($this->session->modal)) {
            echo "swal('".$this->session->title."', '".$this->session->text."', '".$this->session->icon."');";
        }
        ?>
    });
    function loading(){
        document.body.innerHTML = '<div class="loading-screen">\n' +
            '  <div class="loading-icon"><i class="fas fa-sync-alt fa-spin"></i></div>\n' +
            '</div>' + document.body.innerHTML;
    }
</script>
</body>
</html>
