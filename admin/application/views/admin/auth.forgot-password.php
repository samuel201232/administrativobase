<?php $this->load->view('admin/header_login'); ?>
    <style>body{background-image: url('<?=base_url()?>assets/img/bg-login.jpg');background-size: cover;
            background-repeat:no-repeat;}</style>
    <div class="container">
        <form class="sign-in-form" action="<?php echo base_url('index.php/loginctrl/geraSenha');?>" method="post" autocomplete="off">
            <div class="card">
                <div class="card-body">
                    <div class="brand text-center d-block m-b-20">
                        <img src="<?=base_url();?>assets/img/qt-logo.png" alt="Logo" />
                    </div>
                    <h5 class="sign-in-heading text-center">Esqueceu a senha?</h5>
                    <p class="text-center text-muted">Informe o e-mail de cadastro para gerarmos uma nova senha</p>
                    <div class="form-group">
                        <label for="inputEmail" class="sr-only">E-mail</label>
                        <input type="email" id="email" name="email" class="form-control" placeholder="E-mail" required="">
                    </div>
                    <button class="btn btn-primary btn-rounded btn-floating btn-md btn-block" type="submit" id="enviar">Enviar</button>
                    <div class="checkbox text-center m-b-10 m-t-20">
                        <a href="<?=base_url('index.php/loginctrl/index');?>" class="">Lembrou a senha?</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
<?php $this->load->view('admin/footer_login'); ?>
<script>
    $(document).ready(function() {

        <?php
        if (isset($this->session->modal)) {
            echo "swal('".$this->session->title."', '".$this->session->text."', '".$this->session->icon."');";
        }
        ?>

        $('.sign-in-form').validate({
            rules: {
            },
            messages:{
            },
            highlight: function(element) {
                $(element).closest('input').removeClass('success').addClass('error');
                $(element).closest('select').removeClass('success').addClass('error');
                $(element).closest('textarea').removeClass('success').addClass('error');
            },
            success: function(element) {
                element
                    .addClass('valid')
                    .closest('input').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('select').removeClass('error').addClass('success');

                element
                    .addClass('valid')
                    .closest('textarea').removeClass('error').addClass('success');
            },
            submitHandler: function( form ){
                $('#enviar').prop("disabled", true).html('<i class="fas fa-sync-alt fa-spin"></i> Enviando');
                form.submit();
                return false;
            }
        });
    });
</script>
