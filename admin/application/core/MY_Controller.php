<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $CI = &get_instance();
        if (!$this->session->email) {
            redirect(base_url('index.php/loginctrl/index'));
        }
    }
}