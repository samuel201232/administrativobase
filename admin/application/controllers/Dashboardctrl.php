<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboardctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        require_once(BASEPATH . '../application/third_party/google-api-php-client-2.2.2/vendor/autoload.php');
        $json = BASEPATH . '../application/third_party/'.$this->config->item('ga_json');
        putenv('GOOGLE_APPLICATION_CREDENTIALS='.$json);
    }

    function index()
    {
        $this->load->view('admin/index.php');
    }

    function dashboard()
    {
        $posts = $this->input->post();
        $data['startDate'] = (isset($posts['startDate']))?implode("-", array_reverse(explode("/", $posts['startDate']))):date('Y-m-d', strtotime('-30 days'));
        $data['endDate'] = (isset($posts['endDate']))?implode("-", array_reverse(explode("/", $posts['endDate']))):date('Y-m-d');
        $data['googleAnalytics'] = $this->googleAnalytics($data['startDate'],$data['endDate']);
        $this->load->view('admin/dashboard.php',$data);
    }

    function getAuthenticateServiceAccount() {
        try {
            // Create and configure a new client object.
            $http = new GuzzleHttp\Client(['verify' => BASEPATH . '../application/third_party/cacert.pem']);
            $client = new Google_Client();
            $client->setHttpClient($http);
            $client->addScope(Google_Service_Analytics::ANALYTICS_READONLY);
            $client->setAccessType('offline');
            $client->useApplicationDefaultCredentials();
            return new Google_Service_Analytics($client);
        } catch (Exception $e) {
            print "An error occurred: " . $e->getMessage();
        }
    }

    function googleAnalytics($startDate,$endDate)
    {

        $data = array();
        $analytics = $this->getAuthenticateServiceAccount();

        $profileId = "ga:".$this->config->item('ga_id');
        /******** Acessos desktop/mobile/tablet ********/
        /******** pageviews/deviceCategory ********/
        $metrics = "ga:pageviews";
        $optParams = array("dimensions" => "ga:deviceCategory","sort" => "-ga:pageviews");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['deviceCategory'] = $results->rows;
        /******** ******** ********/

        /******** Top 5 acessos pelo navegador ********/
        /******** pageviews/browser ********/
        $metrics = "ga:pageviews";
        $optParams = array("dimensions" => "ga:browser","sort" => "-ga:pageviews","max-results" => 5);
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['browser'] = $results->rows;
        /******** ******** ********/

        /******** Informacao da idade dos usuarios ********/
        /******** users/userAgeBracket ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:userAgeBracket","sort" => "ga:userAgeBracket");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['userAgeBracket'] = $results->rows;
        /******** ******** ********/

        /******** Informacao do genero dos usuarios ********/
        /******** users/userGender ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:userGender","sort" => "ga:userGender");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['userGender'] = $results->rows;
        /******** ******** ********/

        /******** Top 10 cidades acessadas (Brazil) ********/
        /******** users/city (Brazil) ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:city","filters" => "ga:country==Brazil","sort" => "-ga:users","max-results" => 10);
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['city'] = $results->rows;
        /******** ******** ********/

        /******** Top 10 cidades acessadas (Sem Brazil) ********/
        /******** users/city (Brazil) ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:city","filters" => "ga:country!=Brazil","sort" => "-ga:users","max-results" => 10);
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['city_sembr'] = $results->rows;
        /******** ******** ********/

        /******** Numero de usuarios acessando (agrupado por semana) ********/
        /******** users/week ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:yearWeek");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['yearWeek'] = $results->rows;
        /******** ******** ********/

        /******** De onde veio o acesso (Ex. Direto / Rede social) ********/
        /******** users/channelGrouping ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:channelGrouping","sort" => "-ga:users");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['channelGrouping'] = $results->rows;
        /******** ******** ********/

        /******** Top 10 palavras chaves ********/
        /******** users/keyword ********/
        $metrics = "ga:users";
        $optParams = array("dimensions" => "ga:keyword","sort" => "-ga:users","max-results" => 10);
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['keyword'] = $results->rows;
        /******** ******** ********/

        /******** paginas acessadas ********/
        /******** pageviews/pagePath ********/
        $metrics = "ga:pageviews";
        $optParams = array("dimensions" => "ga:pagePath","sort" => "-ga:pageviews");
        $results = $analytics->data_ga->get($profileId, $startDate, $endDate, $metrics, $optParams);
        $data['pagePath'] = $results->rows;
        /******** ******** ********/

        return $data;
    }

}

?>
