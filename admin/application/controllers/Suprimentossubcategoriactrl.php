<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suprimentossubcategoriactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('SuprimentosCategoriamd', '', TRUE);
        $this->load->model('SuprimentosSubcategoriamd', '', TRUE);
    }

    function index()
    {
        $info['suprimentossubcategoria'] = $this->SuprimentosSubcategoriamd->select();
        $this->load->view('admin/suprimentossubcategoria/index.php', $info);

         }
         function select()
         {
             $info['value'] = $this->SuprimentosSubcategoriamd->selectById($this->input->get('id'));
             $info['categorias'] = $this->SuprimentosCategoriamd->select();
             $this->load->view('admin/suprimentossubcategoria/edit.php',$info);
         }
     
         function create()
         {
             $info['categorias'] = $this->SuprimentosCategoriamd->select();
             $this->load->view('admin/suprimentossubcategoria/edit.php', $info);
         }
     
         function insertOrUpdate()
         {
             $posts = $this->input->post();
     
             $hasID = !empty($posts['id']); //used to define if it is an update or insert.
     
             /**** Formatando informacoes ****/
             $posts['slug'] = slug($posts['titulo']);
             $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
             /****************  ****************/
     
             /**** Preparando campos para inserir no banco ****/
             $infoDB = array(
                 'id'                => $posts['id'],
                 'titulo'            => $posts['titulo'],
                 'id_categoria'      => $posts['id_categoria'],
                 'slug'              => $posts['slug'],
                 'ativo'             => $posts['ativo']
             );
             /****************  ****************/
     
             /**** Insert no banco ****/
             $result = ($hasID) ? $this->SuprimentosSubcategoriamd->update($infoDB) : $this->SuprimentosSubcategoriamd->insert($infoDB);
             $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
             gravaLog();
             /****************  ****************/
     
             $successText = 'Dados salvos!';
             $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
             $this->resultModel($result,$successText,$failText,base_url('index.php/suprimentossubcategoriactrl'));
         }
     
         function status()
         {
             $posts = $this->input->post();
             $posts = array_filter($posts);
     
             if(isset($posts['ativo']))
             $posts['ativo'] = ($posts['ativo']=="on")?1:0;
     
             $result = $this->SuprimentosSubcategoriamd->update($posts);
             gravaLog();
             $this->resultModel($result,
             '',
             'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador',
             base_url('index.php/suprimentossubcategoriactrl'));
         }
     
         function delete()
         {
             $result = $this->SuprimentosSubcategoriamd->delete($this->input->post('id'));
             gravaLog();
             $this->resultModel($result,
                 'Registro deletado!',
                 'Erro ao deletar, tente novamente ou entre em contato com o administrador',
                 base_url('index.php/suprimentossubcategoriactrl'));
         }
     
         private function resultModel($result,$successText,$failText,$redirect){
             if(!$result) {
                 // Erro ao salvar os dados
                 $this->session->set_flashdata('modal', '1');
                 $this->session->set_flashdata('icon', 'error');
                 $this->session->set_flashdata('title', 'Erro');
                 $this->session->set_flashdata('text', $failText);
                 redirect($redirect);
             } else {
                 $this->session->set_flashdata('modal', '1');
                 $this->session->set_flashdata('icon', 'success');
                 $this->session->set_flashdata('title', 'Sucesso');
                 $this->session->set_flashdata('text', $successText);
                 redirect($redirect);
             }
         }
   

}