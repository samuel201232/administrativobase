<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Loginctrl extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('loginmd', '', TRUE);
    }

    function index()
    {
        // Se ja houver sessao, envia para o dashboard
        if($this->session->email){
            redirect(base_url('index.php/dashboardctrl'));
        }

        // Tela de login
        $this->load->view('admin/auth.sign-in.php');
    }

    function viewEsqueciSenha()
    {
        // Se ja houver sessao, envia para o dashboard
        if($this->session->email){
            redirect(base_url('index.php/dashboardctrl'));
        }

        // Tela de esqueceu a senha
        $this->load->view('admin/auth.forgot-password.php');
    }

    function geraSenha()
    {
        $result = $this->loginmd->getEmail($this->input->post('email'));
        if(!$result) {
            // Na hora de recuperar a senha, se nao achar o e-mail retorna erro
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'E-mail não encontrado no sistema, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/loginctrl/viewEsqueciSenha'));
        } else {
            // Na hora de recuperar a senha, ao achar o e-mail envia a nova senha gerada
            $this->load->helper('enviaemail');
            $email = $this->input->post('email');
            $novaSenha = rand();
            $result = $this->loginmd->updateSenha($email,$novaSenha);

            if(!$result) {
                // Erro ao gerar a nova senha
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'error');
                $this->session->set_flashdata('title', 'Erro');
                $this->session->set_flashdata('text', 'Erro ao gerar a nova senha, tente novamente ou entre em contato com o administrador');
                redirect(base_url('index.php/loginctrl/viewEsqueciSenha'));
            }

            $assunto = "Recuperando a senha da área administrativa";
            $mensagem = '<p style="margin: 0;font-size: 14px;line-height: 18px;text-align: justify"><span style="font-size: 14px; line-height: 18px;">Olá,</span><br><br></p>
                                        <p style="margin: 0;font-size: 14px;line-height: 18px;text-align: justify"><span style="font-size: 14px; line-height: 18px;">Segue a sua nova credencial:</span><br><br></p>
<p style="margin: 0;font-size: 14px;line-height: 18px;text-align: justify"><span style="font-size: 14px; line-height: 18px;">
<strong>Link:</strong> <a href="'.base_url().'">'.base_url().'</a></span><br>
<strong>Usuário:</strong> '.$email.'</span><br>
<strong>Senha:</strong> '.$novaSenha.'</span><br>
<br></p>';
            if(enviarEmail($email,$assunto,$mensagem) === TRUE) {
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'success');
                $this->session->set_flashdata('title', 'Sucesso');
                $this->session->set_flashdata('text', 'Enviamos um e-mail com a nova senha para <b>'.$this->input->post('email').'<b>');
                redirect(base_url('index.php/loginctrl/index'));
            } else {
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'error');
                $this->session->set_flashdata('title', 'Erro');
                $this->session->set_flashdata('text', 'Tente novamente ou entre em contato com o administrador');
                redirect(base_url('index.php/loginctrl/viewEsqueciSenha'));
            }
        }
    }

    function verificaLogin()
    {
        $result = $this->loginmd->getLogin($this->input->post('email'),$this->input->post('senha'));
        if(!$result) {
            // Login errado, retorna erro
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Credencial inválida, tente novamente ou gere uma nova senha!');
            redirect(base_url('index.php/loginctrl/index'));
        } else {
            // Login correto, grava a sessao e direciona para o dashboard
            $sessaoUsuario = get_object_vars($result[0]); // converte o objeto para array
            $this->session->set_userdata($sessaoUsuario); // grava sessao
            redirect(base_url('index.php/dashboardctrl'));
        }
    }

    function logOut()
    {
        $this->session->sess_destroy();
        redirect(base_url('index.php/loginctrl/index'));
    }

}

?>
