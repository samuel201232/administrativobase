<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pecasctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Pecasmd', '', TRUE);
        $this->load->model('PecasCategoriamd', '', TRUE);
        $this->load->model('Marcamd', '', TRUE);
        $this->load->model('PecasSubcategoriamd', '', TRUE);
        $this->load->library('upload');
    }

    function index()
    {
        $info['pecas'] = $this->Pecasmd->select();
        $this->load->view('admin/pecas/index.php',$info);
    }

    function select()
    {
        $info['value'] = $this->Pecasmd->selectById($this->input->get('id'));
        $info['galeria'] = $this->Pecasmd->selectGaleriaById($info['value'][0]->id);
        $info['categorias'] = $this->PecasCategoriamd->select();
        $info['marcas'] = $this->Marcamd->select();
        $info['subcategorias'] = $this->PecasSubcategoriamd->select();
        $this->load->view('admin/pecas/edit.php',$info);
    }

    function create()
    {
        $info['marcas'] = $this->Marcamd->select();
        $info['categorias'] = $this->PecasCategoriamd->select();
        $this->load->view('admin/pecas/edit.php',$info);
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();
        
        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        $posts['destaque'] = (isset($posts['destaque'])&&$posts['destaque']=="on")?1:0;
        $posts['usado'] = (isset($posts['usado'])&&$posts['usado']=="on")?1:0;
        /****************  ****************/

        /** Upload Single Images */
        $imagem_principal = $this->fileUpload($this->Pecasmd,'imagem_principal','img/pecas/','jpg|jpeg|gif|png',($hasID) ? $posts['id'] : NULL);
        $arquivo = $this->fileUpload($this->Pecasmd,'arquivo','file/pecas/','jpg|jpeg|gif|png',($hasID) ? $posts['id'] : NULL);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                        => $posts['id'],
            'id_marca'      		    => $posts['id_marca'],
            'id_categoria'              => $posts['id_categoria'],
            'id_subcategoria'           => $posts['id_subcategoria'],
            'titulo'                    => $posts['titulo'],
            'usado'                     => $posts['usado'],
            'resumo'                    => $posts['resumo'],
            'descricao'                 => $posts['descricao'],
            'visao_geral'               => $posts['visao_geral'],
            'especificacoes_tecnicas'   => $posts['especificacoes_tecnicas'],
            'acessorios'                => $posts['acessorios'],
            'garantia'                  => $posts['garantia'],
            'slug'                      => $posts['slug'],
            'imagem_principal'          => $imagem_principal,
            'arquivo'                   => $arquivo,
            'ativo'                     => $posts['ativo'],
            'destaque'                  => $posts['destaque'],
        );
        /****************  ****************/


        /**** Insert no banco ****/
        $result = ($hasID) ? $this->Pecasmd->update($infoDB) : $this->Pecasmd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        $result_galeria = $this->handleGallery($this->Pecasmd,$posts,$id,'id_suprimentos',$hasID);
        
        $successText = 'Dados salvos!';
        $failText = (!$result) ? 
            'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador':
            'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador ';
        $this->resultModel(($result_galeria && $result),$successText,$failText,base_url('index.php/Pecasctrl'));
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;
        
        $result = $this->Suprimentosmd->update($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/Pecasctrl'));
        }
    }

    function delete()
    {
        /**** Deleta imagens da galeria ****/
        $galeria = $this->Pecasmd->selectGaleriaById($this->input->post('id'));
        if($galeria){
            foreach ($galeria as $img){
                deleteImg($img->imagem);
            }
            $this->Pecasmd->deleteGaleria($this->input->post('id'));
        }
        /****************  ****************/

        $result = $this->Pecasmd->delete($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/Pecasctrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/Pecasctrl'));
        }
    }

    function order()
    {
       $info['pecas'] = $this->Pecasmd->select();
       $this->load->view('admin/pecas/order.php',$info);
    }

    function AjaxOrder()
    {
        $posts = $this->input->post();
        $posts = $posts['item'];
        echo "count :".count($posts);
        print_r($posts);
        for ($i = 0; $i < count($posts); $i++) {
            $info = array(
                'id'  => $posts[$i],
                'ordem' => $i,
            );
            print_r($info);
            $this->Pecasmd->update($info);
        }
    }

    function SaveOrder()
    {
        /*
         * Apenas para dar uma mensagem bonita pro usuário
         */
        $this->session->set_flashdata('modal', '1');
        $this->session->set_flashdata('icon', 'success');
        $this->session->set_flashdata('title', 'Sucesso');
        $this->session->set_flashdata('text', 'Dados salvos!');
        redirect(base_url('index.php/Pecasctrl'));
    }

    function getSubcategoriaByCategoria()
    {
        $categoria = $this->input->post('categoria');
        $info['value'] = $this->Pecasmd->selectByCategoria($categoria);
        print_r(json_encode($info));
    }

    private function resultModel($result,$successText,$failText,$redirect){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect($redirect);
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect($redirect);
        }
    }

    private function handleGallery($model,$posts,$id,$foreign,$isUpdate){
        $infoDB = array();
        $galeria = isset($posts['imagem']) ? $posts['imagem'] : NULL;
        $legenda = isset($posts['legenda']) ? $posts['legenda'] : NULL;
        $result_galeria= true;

        if(!empty($galeria)){
            foreach ($galeria as $key => $value){
                $infoDB[$key][$foreign] = $id;
                $infoDB[$key]['imagem'] = $value;
                $infoDB[$key]['legenda'] = $legenda[$key];
            }
            if($isUpdate) $model->deleteGaleria($id);
            $result_galeria = $model->insertGaleria($infoDB);
            gravaLog();
        }
        
        return $result_galeria;
    }

    private function fileUpload($model,$inputName,$uploadPath,$allowedTypes,$id = NULL){
        $config = [
            'upload_path' => 'public/'.$uploadPath,
            'allowed_types' => $allowedTypes
        ];
        $this->upload->initialize($config);

        // Gets current image stored on entry (during updates) or empty on new entries.
        $currentImg = !is_null($id) ? $model->selectById($id)[0]->$inputName : '';
        
        // Output file name. If there is an input file, get its name, otherwise we keep the current image.
        $img_output = (isset($_FILES[$inputName]) && !empty($_FILES[$inputName]['name'])) ? $uploadPath.$_FILES[$inputName]['name'] : $currentImg;

        // File to be deleted in case of an image input during update.
        $toDelete = FCPATH.'public/'.$currentImg;

        if(empty($currentImg) && !empty($img_output)){ // If we are adding new entry (or there is no image on updated entry) and an input was given.
            $this->upload->do_upload($inputName);
        }else if($img_output != $currentImg){ // If there is a current image file and a new file input was given
            //unlink($toDelete);
            $this->upload->do_upload($inputName);
        }

        return $img_output;
    }
}