<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Servicoctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Servicomd', '', TRUE);
    }

    function index()
    {
        $info['servico'] = $this->Servicomd->selectServico();
        $this->load->view('admin/servico/index.php',$info);
    }

    function selectServico()
    {
        $info['value'] = $this->Servicomd->selectServicoById($this->input->get('id'));
        $info['galeria'] = $this->Servicomd->selectServicoGaleriaById($info['value'][0]->id);
        $this->load->view('admin/servico/edit.php',$info);
    }

    function createServico()
    {
        $this->load->view('admin/servico/edit.php');
    }

    function insertServico()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        $posts['id_graduacao'] = implode (";", $posts['id_graduacao']);
        $posts['id_posgraduacao'] = implode (";", $posts['id_posgraduacao']);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'titulo'            => $posts['titulo'],
            'resumo'            => $posts['resumo'],
            'descricao'         => $posts['descricao'],
            'slug'              => $posts['slug'],
            'imagem_principal'  => $posts['imagem_principal'],
            'imagem_topo'       => $posts['imagem_topo'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = $this->Servicomd->insertServico($infoDB);
        $lastId = $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/servicoctrl'));
        } else {
            //Dados inseridos corretamente
            $infoDB = array();
            $galeria = $posts['imagem'];
            $legenda = $posts['legenda'];

            if(is_array($galeria)){
                //Existe galeria de imagem, salvando no banco
                foreach ($galeria as $key => $value){
                    $infoDB[$key]['id_servico'] = $lastId;
                    $infoDB[$key]['imagem'] = $value;
                    $infoDB[$key]['legenda'] = $legenda[$key];
                }
                $result_galeria = $this->Servicomd->insertServicoGaleria($infoDB);
                gravaLog();
                if(!$result_galeria) {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'error');
                    $this->session->set_flashdata('title', 'Erro');
                    $this->session->set_flashdata('text', 'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador');
                    redirect(base_url('index.php/servicoctrl'));
                } else {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'success');
                    $this->session->set_flashdata('title', 'Sucesso');
                    $this->session->set_flashdata('text', 'Dados salvos!');
                    redirect(base_url('index.php/servicoctrl'));
                }
            } else {
                //Nao existe galeria de imagem, mensagem de sucesso
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'success');
                $this->session->set_flashdata('title', 'Sucesso');
                $this->session->set_flashdata('text', 'Dados salvos!');
                redirect(base_url('index.php/servicoctrl'));
            }


        }
    }

    function updateServico()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        $posts['id_graduacao'] = implode (";", $posts['id_graduacao']);
        $posts['id_posgraduacao'] = implode (";", $posts['id_posgraduacao']);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                    => $posts['id'],
            'titulo'                => $posts['titulo'],
            'resumo'                => $posts['resumo'],
            'descricao'             => $posts['descricao'],
            'slug'                  => $posts['slug'],
            'imagem_principal'      => $posts['imagem_principal'],
            'imagem_topo'           => $posts['imagem_topo'],
            'ativo'                 => $posts['ativo']
        );
        /****************  ****************/

        /**** Update no banco ****/
        $result = $this->Servicomd->updateServico($infoDB);
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/servicoctrl'));
        } else {
            //Dados inseridos corretamente
            $lastId = $posts['id'];
            $infoDB = array();
            $galeria = $posts['imagem'];
            $legenda = $posts['legenda'];

            if(is_array($galeria)){
                //Existe galeria de imagem, salvando no banco
                foreach ($galeria as $key => $value){
                    $infoDB[$key]['id_servico'] = $lastId;
                    $infoDB[$key]['imagem'] = $value;
                    $infoDB[$key]['legenda'] = $legenda[$key];
                }
                $this->Servicomd->deleteServicoGaleria($lastId);
                $result_galeria = $this->Servicomd->insertServicoGaleria($infoDB);
                gravaLog();
                if(!$result_galeria) {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'error');
                    $this->session->set_flashdata('title', 'Erro');
                    $this->session->set_flashdata('text', 'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador');
                    redirect(base_url('index.php/servicoctrl'));
                } else {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'success');
                    $this->session->set_flashdata('title', 'Sucesso');
                    $this->session->set_flashdata('text', 'Dados salvos!');
                    redirect(base_url('index.php/servicoctrl'));
                }
            } else {
                //Por nao ter nenhuma galeria, deleta o que houver no banco
                $this->Servicomd->deleteServicoGaleria($lastId);

                //Nao existe galeria de imagem, mensagem de sucesso
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'success');
                $this->session->set_flashdata('title', 'Sucesso');
                $this->session->set_flashdata('text', 'Dados salvos!');
                redirect(base_url('index.php/servicoctrl'));
            }
        }
    }

    function statusServico()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->Servicomd->updateServico($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/servicoctrl'));
        }
    }

    function deleteServico()
    {
        /**** Deleta imagens da galeria ****/
        $galeria = $this->Servicomd->selectServicoGaleriaById($this->input->post('id'));
        if($galeria){
            foreach ($galeria as $img){
                deleteImg($img->imagem);
            }
            $this->Servicomd->deleteServicoGaleria($this->input->post('id'));
        }
        /****************  ****************/

        $result = $this->Servicomd->deleteServico($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/servicoctrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/servicoctrl'));
        }
    }

}

?>
