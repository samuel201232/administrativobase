<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProdutoSubcategoriactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ProdutoCategoriamd', '', TRUE);
        $this->load->model('ProdutoSubcategoriamd', '', TRUE);
    }

    function index()
    {
        $info['produtosubcategoria'] = $this->ProdutoSubcategoriamd->select();
        $this->load->view('admin/produtosubcategoria/index.php', $info);
    }

    function select()
    {
        $info['value'] = $this->ProdutoSubcategoriamd->selectById($this->input->get('id'));
        $info['categorias'] = $this->ProdutoCategoriamd->select();
        $this->load->view('admin/produtosubcategoria/edit.php',$info);
    }

    function create()
    {
        $info['categorias'] = $this->ProdutoCategoriamd->select();
        $this->load->view('admin/produtosubcategoria/edit.php', $info);
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();

        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                => $posts['id'],
            'titulo'            => $posts['titulo'],
            'id_categoria'      => $posts['id_categoria'],
            'slug'              => $posts['slug'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = ($hasID) ? $this->ProdutoSubcategoriamd->update($infoDB) : $this->ProdutoSubcategoriamd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        $successText = 'Dados salvos!';
        $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText);
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->ProdutoSubcategoriamd->update($posts);
        gravaLog();

        $successText = '';
        $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText);
    }

    function delete()
    {
        $result = $this->ProdutoSubcategoriamd->delete($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/ProdutoSubcategoriactrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/ProdutoSubcategoriactrl'));
        }
        $successText = 'Registro deletado!';
        $failText = 'Erro ao deletar, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText);
    }

    private function resultModel($result,$successText,$failText){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect(base_url('index.php/produtosubcategoriactrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect(base_url('index.php/produtosubcategoriactrl'));
        }
    }

}