<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Marcactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Marcamd', '', TRUE);
        $this->load->library('upload');
    }

    function index()
    {
        $info['marca'] = $this->Marcamd->select();
        $this->load->view('admin/marca/index.php', $info);
    }

    function select()
    {
        $info['value'] = $this->Marcamd->selectById($this->input->get('id'));
        $this->load->view('admin/marca/edit.php',$info);
    }

    function create()
    {
        $this->load->view('admin/marca/edit.php');
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();

        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /** Upload Single Images */
        $imagem_principal = $this->fileUpload($this->Marcamd,'imagem_principal','img/marcas/','jpg|jpeg|gif|png',($hasID) ? $posts['id'] : NULL);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                => $posts['id'],
            'titulo'            => $posts['titulo'],
            'imagem_principal'  => $imagem_principal,
            'slug'              => $posts['slug'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = ($hasID) ? $this->Marcamd->update($infoDB) : $this->Marcamd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        $successText = 'Dados salvos!';
        $failText = (!$result) ? 
            'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador':
            'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador ';
        $this->resultModel($result,$successText,$failText,base_url('index.php/Marcactrl'));
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->Marcamd->update($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/Marcactrl'));
        }
    }

    function delete()
    {
        $result = $this->Marcamd->delete($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/Marcactrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/Marcactrl'));
        }
    }

    function order()
    {
        $info['marca'] = $this->Marcamd->select();
        $this->load->view('admin/marca/order.php',$info);
    }

    function AjaxOrder()
    {
        $posts = $this->input->post();
        $posts = $posts['item'];
        echo "count :".count($posts);
        print_r($posts);
        for ($i = 0; $i < count($posts); $i++) {
            $info = array(
                'id'  => $posts[$i],
                'ordem' => $i,
            );
            print_r($info);
            $this->Marcamd->update($info);
        }
    }

    function SaveOrder()
    {
        /*
         * Apenas para dar uma mensagem bonita pro usuário
         */
        $this->session->set_flashdata('modal', '1');
        $this->session->set_flashdata('icon', 'success');
        $this->session->set_flashdata('title', 'Sucesso');
        $this->session->set_flashdata('text', 'Dados salvos!');
        redirect(base_url('index.php/marcactrl'));
    }

    private function resultModel($result,$successText,$failText,$redirect){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect($redirect);
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect($redirect);
        }
    }

    private function fileUpload($model,$inputName,$uploadPath,$allowedTypes,$id = NULL){
        $config = [
            'upload_path' => 'public/'.$uploadPath,
            'allowed_types' => $allowedTypes
        ];
        $this->upload->initialize($config);

        // Gets current image stored on entry (during updates) or empty on new entries.
        $currentImg = !is_null($id) ? $model->selectById($id)[0]->$inputName : '';
        
        // Output file name. If there is an input file, get its name, otherwise we keep the current image.
        $img_output = (isset($_FILES[$inputName]) && !empty($_FILES[$inputName]['name'])) ? $uploadPath.$_FILES[$inputName]['name'] : $currentImg;

        // File to be deleted in case of an image input during update.
        $toDelete = FCPATH.'public/'.$currentImg;

        if(empty($currentImg) && !empty($img_output)){ // If we are adding new entry (or there is no image on updated entry) and an input was given.
            $this->upload->do_upload($inputName);
        }else if($img_output != $currentImg){ // If there is a current image file and a new file input was given
            //unlink($toDelete);
            $this->upload->do_upload($inputName);
        }

        return $img_output;
    }
}