<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProdutoCategoriactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('ProdutoCategoriamd', '', TRUE);
    }

    function index()
    {
        $info['produtocategoria'] = $this->ProdutoCategoriamd->select();
        $this->load->view('admin/produtocategoria/index.php', $info);
    }

    function select()
    {
        $info['value'] = $this->ProdutoCategoriamd->selectById($this->input->get('id'));
        $this->load->view('admin/produtocategoria/edit.php',$info);
    }

    function create()
    {
        $this->load->view('admin/produtocategoria/edit.php');
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();

        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                => $posts['id'],
            'titulo'            => $posts['titulo'],
            'slug'              => $posts['slug'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = ($hasID) ? $this->ProdutoCategoriamd->update($infoDB) : $this->ProdutoCategoriamd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/
        
        $successText = 'Dados salvos!';
        $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText);
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->ProdutoCategoriamd->update($posts);
        gravaLog();

        $this->resultModel($result,'','Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
    }

    function delete()
    {
        $result = $this->ProdutoCategoriamd->delete($this->input->post('id'));
        gravaLog();
        $successText = 'Registro deletado!';
        $failText = 'Erro ao deletar, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText);
    }

    private function resultModel($result,$successText,$failText){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect(base_url('index.php/produtocategoriactrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect(base_url('index.php/produtocategoriactrl'));
        }
    }

}