<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticiactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Noticiamd', '', TRUE);
    }

    function index()
    {
        $info['noticia'] = $this->Noticiamd->selectNoticia();
        $this->load->view('admin/noticia/index.php',$info);
    }

    function selectNoticia()
    {
        $info['value'] = $this->Noticiamd->selectNoticiaById($this->input->get('id'));
        $info['galeria'] = $this->Noticiamd->selectNoticiaGaleriaById($info['value'][0]->id);
        $this->load->view('admin/noticia/edit.php',$info);
    }

    function createNoticia()
    {
        $this->load->view('admin/noticia/edit.php');
    }

    function insertNoticia()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['dia'] = implode("-", array_reverse(explode("/", $posts['dia'])));
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        $posts['id_graduacao'] = implode (";", $posts['id_graduacao']);
        $posts['id_posgraduacao'] = implode (";", $posts['id_posgraduacao']);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'titulo'            => $posts['titulo'],
            'resumo'            => $posts['resumo'],
            'dia'               => $posts['dia'],
            'descricao'         => $posts['descricao'],
            'slug'              => $posts['slug'],
            'imagem_principal'  => $posts['imagem_principal'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = $this->Noticiamd->insertNoticia($infoDB);
        $lastId = $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/noticiactrl'));
        } else {
            //Dados inseridos corretamente
            $infoDB = array();
            $galeria = $posts['imagem'];
            $legenda = $posts['legenda'];

            if(is_array($galeria)){
                //Existe galeria de imagem, salvando no banco
                foreach ($galeria as $key => $value){
                    $infoDB[$key]['id_noticia'] = $lastId;
                    $infoDB[$key]['imagem'] = $value;
                    $infoDB[$key]['legenda'] = $legenda[$key];
                }
                $result_galeria = $this->Noticiamd->insertNoticiaGaleria($infoDB);
                gravaLog();
                if(!$result_galeria) {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'error');
                    $this->session->set_flashdata('title', 'Erro');
                    $this->session->set_flashdata('text', 'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador');
                    redirect(base_url('index.php/noticiactrl'));
                } else {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'success');
                    $this->session->set_flashdata('title', 'Sucesso');
                    $this->session->set_flashdata('text', 'Dados salvos!');
                    redirect(base_url('index.php/noticiactrl'));
                }
            } else {
                //Nao existe galeria de imagem, mensagem de sucesso
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'success');
                $this->session->set_flashdata('title', 'Sucesso');
                $this->session->set_flashdata('text', 'Dados salvos!');
                redirect(base_url('index.php/noticiactrl'));
            }


        }
    }

    function updateNoticia()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['dia'] = implode("-", array_reverse(explode("/", $posts['dia'])));
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        $posts['id_graduacao'] = implode (";", $posts['id_graduacao']);
        $posts['id_posgraduacao'] = implode (";", $posts['id_posgraduacao']);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                    => $posts['id'],
            'titulo'                => $posts['titulo'],
            'resumo'                => $posts['resumo'],
            'dia'                   => $posts['dia'],
            'descricao'             => $posts['descricao'],
            'slug'                  => $posts['slug'],
            'imagem_principal'      => $posts['imagem_principal'],
            'ativo'                 => $posts['ativo']
        );
        /****************  ****************/

        /**** Update no banco ****/
        $result = $this->Noticiamd->updateNoticia($infoDB);
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/noticiactrl'));
        } else {
            //Dados inseridos corretamente
            $lastId = $posts['id'];
            $infoDB = array();
            $galeria = $posts['imagem'];
            $legenda = $posts['legenda'];

            if(is_array($galeria)){
                //Existe galeria de imagem, salvando no banco
                foreach ($galeria as $key => $value){
                    $infoDB[$key]['id_noticia'] = $lastId;
                    $infoDB[$key]['imagem'] = $value;
                    $infoDB[$key]['legenda'] = $legenda[$key];
                }
                $this->Noticiamd->deleteNoticiaGaleria($lastId);
                $result_galeria = $this->Noticiamd->insertNoticiaGaleria($infoDB);
                gravaLog();
                if(!$result_galeria) {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'error');
                    $this->session->set_flashdata('title', 'Erro');
                    $this->session->set_flashdata('text', 'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador');
                    redirect(base_url('index.php/noticiactrl'));
                } else {
                    $this->session->set_flashdata('modal', '1');
                    $this->session->set_flashdata('icon', 'success');
                    $this->session->set_flashdata('title', 'Sucesso');
                    $this->session->set_flashdata('text', 'Dados salvos!');
                    redirect(base_url('index.php/noticiactrl'));
                }
            } else {
                //Por nao ter nenhuma galeria, deleta o que houver no banco
                $this->Noticiamd->deleteNoticiaGaleria($lastId);

                //Nao existe galeria de imagem, mensagem de sucesso
                $this->session->set_flashdata('modal', '1');
                $this->session->set_flashdata('icon', 'success');
                $this->session->set_flashdata('title', 'Sucesso');
                $this->session->set_flashdata('text', 'Dados salvos!');
                redirect(base_url('index.php/noticiactrl'));
            }
        }
    }

    function statusNoticia()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->Noticiamd->updateNoticia($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/noticiactrl'));
        }
    }

    function deleteNoticia()
    {
        /**** Deleta imagens da galeria ****/
        $galeria = $this->Noticiamd->selectNoticiaGaleriaById($this->input->post('id'));
        if($galeria){
            foreach ($galeria as $img){
                deleteImg($img->imagem);
            }
            $this->Noticiamd->deleteNoticiaGaleria($this->input->post('id'));
        }
        /****************  ****************/

        $result = $this->Noticiamd->deleteNoticia($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/noticiactrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/noticiactrl'));
        }
    }

}

?>
