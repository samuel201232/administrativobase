<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Produtoctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
		$this->load->model('Produtomd', '', TRUE);
        $this->load->model('ProdutoCategoriamd', '', TRUE);
		$this->load->model('Marcamd', '', TRUE);
        $this->load->model('ProdutoSubcategoriamd', '', TRUE);
        $this->load->library('upload');
    }

    function index()
    {
        $info['produto'] = $this->Produtomd->select();
        $this->load->view('admin/produto/index.php',$info);
    }

    function select()
    {
        $info['value'] = $this->Produtomd->selectById($this->input->get('id'));
		$info['galeria'] = $this->Produtomd->selectGaleriaById($info['value'][0]->id);
        $info['categorias'] = $this->ProdutoCategoriamd->select();
        $info['marcas'] = $this->Marcamd->select();
        $info['subcategorias'] = $this->ProdutoSubcategoriamd->select();
        $this->load->view('admin/produto/edit.php',$info);
    }

    function create()
    
    {
        $info['marcas'] = $this->Marcamd->select();
        $info['categorias'] = $this->ProdutoCategoriamd->select();
        $this->load->view('admin/produto/edit.php',$info);
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();

        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
		$posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
		$posts['destaque'] = (isset($posts['destaque'])&&$posts['destaque']=="on")?1:0;
		$posts['usado'] = (isset($posts['usado'])&&$posts['usado']=="on")?1:0;
        /****************  ****************/

        /** Upload Single Images */
        $imagem_principal = $this->fileUpload($this->Produtomd,'imagem_principal','img/produtos/','jpg|jpeg|gif|png',($hasID) ? $posts['id'] : NULL);
        $arquivo = $this->fileUpload($this->Produtomd,'arquivo','files/produtos','*',($hasID) ? $posts['id'] : NULL);
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                        => $posts['id'],
            'id_marca'      		    => $posts['id_marca'],
            'id_categoria'      		=> $posts['id_categoria'],
            'id_subcategoria'      		=> $posts['id_subcategoria'],
			'titulo'            		=> $posts['titulo'],
			'usado' 					=> $posts['usado'],
            'resumo'            		=> $posts['resumo'],
			'descricao'         		=> $posts['descricao'],
			'visao_geral'         		=> $posts['visao_geral'],
			'especificacoes_tecnicas'   => $posts['especificacoes_tecnicas'],
			'acessorios'         		=> $posts['acessorios'],
			'garantia'         			=> $posts['garantia'],
            'slug'              		=> $posts['slug'],
            'imagem_principal'  		=> $imagem_principal,
            'arquivo'       		    => $arquivo,
			'ativo'             		=> $posts['ativo'],
			'destaque'             		=> $posts['destaque'],
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = ($hasID) ? $this->Produtomd->update($infoDB) : $this->Produtomd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        $result_galeria = $this->handleGallery($this->Produtomd,$posts,$id,'id_produto',$hasID);
        
        $successText = 'Dados salvos!';
        $failText = (!$result) ? 
            'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador':
            'Erro ao salvar as imagens, tente novamente ou entre em contato com o administrador ';
        $this->resultModel(($result_galeria && $result),$successText,$failText,base_url('index.php/produtoctrl'));
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
		$posts['ativo'] = ($posts['ativo']=="on")?1:0;		
        $result = $this->Produtomd->update($posts);
        gravaLog();
        $this->resultModel($result,
            '',
            'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador',
            base_url('index.php/produtoctrl'));
    }

    function delete()
    {
        /**** Deleta imagens da galeria ****/
        $galeria = $this->Produtomd->selectGaleriaById($this->input->post('id'));
        if($galeria){
            foreach ($galeria as $img){
                deleteImg($img->imagem);
            }
            $this->Produtomd->deleteGaleria($this->input->post('id'));
        }
        /****************  ****************/

        $result = $this->Produtomd->delete($this->input->post('id'));
        gravaLog();
        $this->resultModel($result,
            'Registro deletado!',
            'Erro ao deletar, tente novamente ou entre em contato com o administrador',
            base_url('index.php/produtoctrl'));

    }

    function order()
    {
        $info['produto'] = $this->Produtomd->select();
        $this->load->view('admin/produto/order.php',$info);
    }

    function AjaxOrder()
    {
        $posts = $this->input->post();
        $posts = $posts['item'];
        echo "count :".count($posts);
        print_r($posts);
        for ($i = 0; $i < count($posts); $i++) {
            $info = array(
                'id'  => $posts[$i],
                'ordem' => $i,
            );
            print_r($info);
            $this->Produtomd->update($info);
        }
    }

    function SaveOrder()
    {
        /*
         * Apenas para dar uma mensagem bonita pro usu�rio
         */
        $this->session->set_flashdata('modal', '1');
        $this->session->set_flashdata('icon', 'success');
        $this->session->set_flashdata('title', 'Sucesso');
        $this->session->set_flashdata('text', 'Dados salvos!');
        redirect(base_url('index.php/produtoctrl'));
    }

    function getSubcategoriaByCategoria()
    {
        $categoria = $this->input->post('categoria');
        $info['value'] = $this->ProdutoSubcategoriamd->selectByCategoria($categoria);
        print_r(json_encode($info));
    }

    private function resultModel($result,$successText,$failText,$redirect){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect($redirect);
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect($redirect);
        }
    }

    private function handleGallery($model,$posts,$id,$foreign,$isUpdate){
        $infoDB = array();
        $galeria = isset($posts['imagem']) ? $posts['imagem'] : NULL;
        $legenda = isset($posts['legenda']) ? $posts['legenda'] : NULL;
        $result_galeria= true;

        if(!empty($galeria)){
            foreach ($galeria as $key => $value){
                $infoDB[$key][$foreign] = $id;
                $infoDB[$key]['imagem'] = $value;
                $infoDB[$key]['legenda'] = $legenda[$key];
            }
            if($isUpdate) $model->deleteGaleria($id);
            $result_galeria = $model->insertGaleria($infoDB);
            gravaLog();
        }
        
        return $result_galeria;
    }

    private function fileUpload($model,$inputName,$uploadPath,$allowedTypes,$id = NULL){
        $config = [
            'upload_path' => 'public/'.$uploadPath,
            'allowed_types' => $allowedTypes
        ];
        $this->upload->initialize($config);

        // Gets current image stored on entry (during updates) or empty on new entries.
        $current = !is_null($id) ? $model->selectById($id)[0]->$inputName : '';
        
        // Output file name. If there is an input file, get its name, otherwise we keep the current image.
        $output = (isset($_FILES[$inputName]) && !empty($_FILES[$inputName]['name'])) ? preg_replace('/\s+/', '_', $uploadPath.$_FILES[$inputName]['name']) : $current;

        // File to be deleted in case of an image input during update.
        $toDelete = FCPATH.'public/'.$current;

        if(empty($current) && !empty($output)){ // If we are adding new entry (or there is no image on updated entry) and an input was given.
            $this->upload->do_upload($inputName);
        }else if($output != $current){ // If there is a current image file and a new file input was given
            unlink($toDelete);
            $this->upload->do_upload($inputName);
        }

        return $output;
    }
}