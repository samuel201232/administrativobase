<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dropzonectrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
    }

    function delete()
    {
        $arquivo = './public/img/'.$this->input->get('id');
        $arquivo_thumb = './public/img/thumbnail/'.$this->input->get('id');
        unlink($arquivo);
        unlink($arquivo_thumb);
    }

    /**
     * Cria Thumbnail
     */
    public function resizeImage($filename)
    {
        $source_path = './public/img/' . $filename;
        $target_path = './public/img/thumbnail/';
        $config_manip = array(
            'image_library' => 'gd2',
            'source_image' => $source_path,
            'new_image' => $target_path,
            'maintain_ratio' => TRUE,
            'create_thumb' => TRUE,
            'thumb_marker' => '',
            'width' => 400,
            'height' => 400
        );

        $this->load->library('image_lib', $config_manip);
        if (!$this->image_lib->resize()) {
            echo $this->image_lib->display_errors();
        }
        $this->image_lib->clear();
    }

    function index()
    {
        $UploadDirectory	= './public/img/';
        $json = array();
        foreach ($_FILES as $key => $value) {

            if (!isset($_SERVER['HTTP_X_REQUESTED_WITH'])){
                die();
            }

            switch(strtolower($_FILES[$key]['type']))
            {
                case 'image/png':
                case 'image/gif':
                case 'image/jpeg':
                case 'image/pjpeg':
                case 'text/plain':
                case 'text/html': //html file
                case 'application/x-zip-compressed':
                case 'application/pdf':
                case 'application/msword':
                case 'application/vnd.ms-excel':
                case 'video/mp4':
                    break;
                default:
                    die('Não tem Suporte para esse arquivo!');
            }

            $File_Name          = strtolower($_FILES[$key]['name']);
            $parts = explode( ".", $_FILES[$key]['name'] );
            $File_Name_No_Ext = $parts[0];
            $File_Ext           = substr($File_Name, strrpos($File_Name, '.'));
            $NewFileName 		= $File_Name_No_Ext.$File_Ext;

            if(move_uploaded_file($_FILES[$key]['tmp_name'], $UploadDirectory.$NewFileName ))
            {
                $json[] = $NewFileName;
                $this->resizeImage($NewFileName);
            }else{
                die('error durante o upload!');
            }
        }
        echo json_encode($json);
    }

}

?>
