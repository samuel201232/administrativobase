<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfilctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('perfilmd', '', TRUE);
    }

    function updatePerfil()
    {
        $posts = $this->input->post();
        unset($posts['senha_confirmar']);
        $posts['senha'] = $posts['senha'] ? md5($posts['senha']) : '';
        $posts = array_filter($posts);
        $result = $this->perfilmd->updatePerfil($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/perfilctrl'));
        } else {
            $this->session->set_userdata($posts); // grava sessao
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Dados salvos!');
            redirect(base_url('index.php/dashboardctrl'));
        }
    }

    function index()
    {
        $info['perfil'] = $this->perfilmd->getLoginById($this->session->id);
        $this->load->view('admin/perfil.php',$info);
    }

}

?>
