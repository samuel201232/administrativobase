<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bannerctrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Bannermd', '', TRUE);
    }

    function index()
    {
        $info['banner'] = $this->Bannermd->selectBanner();
        $this->load->view('admin/banner/index.php', $info);
    }

    function selectBanner()
    {
        $info['value'] = $this->Bannermd->selectBannerById($this->input->get('id'));
        $this->load->view('admin/banner/edit.php',$info);
    }

    function createBanner()
    {
        $this->load->view('admin/banner/edit.php');
    }

    function insertBanner()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['link'] = addhttp($posts['link']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'link'              => $posts['link'],
            'imagem_principal'  => $posts['imagem_principal'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = $this->Bannermd->insertBanner($infoDB);
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/bannerctrl'));
        } else {
            //Dados inseridos corretamente
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Dados salvos!');
            redirect(base_url('index.php/bannerctrl'));
        }
    }

    function updateBanner()
    {
        $posts = $this->input->post();

        /**** Formatando informacoes ****/
        $posts['link'] = addhttp($posts['link']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                => $posts['id'],
            'link'              => $posts['link'],
            'imagem_principal'  => $posts['imagem_principal'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Update no banco ****/
        $result = $this->Bannermd->updateBanner($infoDB);
        gravaLog();
        /****************  ****************/

        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/bannerctrl'));
        } else {
            //Dados inseridos corretamente
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Dados salvos!');
            redirect(base_url('index.php/bannerctrl'));
        }
    }

    function statusBanner()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->Bannermd->updateBanner($posts);
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/bannerctrl'));
        }
    }

    function deleteBanner()
    {
        $result = $this->Bannermd->deleteBanner($this->input->post('id'));
        gravaLog();
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', 'Erro ao deletar, tente novamente ou entre em contato com o administrador');
            redirect(base_url('index.php/bannerctrl'));
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', 'Registro deletado!');
            redirect(base_url('index.php/bannerctrl'));
        }
    }

}

?>
