<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class DbbackupCtrl extends CI_Controller {

    private $pastaBackup = './dbbackup/';

    function __construct()
    {
        parent::__construct();
        $this->deletaAntigo();
    }

    function deletaAntigo(){
        $days = 30;
        $path = $this->pastaBackup;

        // Open the directory
        if ($handle = opendir($path))
        {
            // Loop through the directory
            while (false !== ($file = readdir($handle)))
            {
                // Check the file we're doing is actually a file
                if (is_file($path.$file))
                {
                    // Check if the file is older than X days old
                    if (filemtime($path.$file) < ( time() - ( $days * 24 * 60 * 60 ) ) )
                    {
                        // Do the deletion
                        unlink($path.$file);
                    }
                }
            }
        }
    }

    function index(){
        // Load the DB utility class
        $this->load->dbutil();
        $prefs = array(
            'format'      => 'zip',
            'filename'    => 'db_backup.sql'
        );

        $backup =& $this->dbutil->backup($prefs);

        $db_name = 'backup-em-'. date("Y-m-d-H-i-s") .'.zip';
        $save = $this->pastaBackup.$db_name;

        $this->load->helper('file');
        write_file($save, $backup);

    }

}

?>
