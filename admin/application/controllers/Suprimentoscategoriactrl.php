<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Suprimentoscategoriactrl extends MY_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('SuprimentosCategoriamd', '', TRUE);
    }

    function index()
    {
        $info['suprimentoscategoria'] = $this->SuprimentosCategoriamd->select();
        $this->load->view('admin/suprimentoscategoria/index.php', $info);
    }

    function select()
    {
        $info['value'] = $this->SuprimentosCategoriamd->selectById($this->input->get('id'));
        $this->load->view('admin/suprimentoscategoria/edit.php',$info);
    }

    function create()
    {
        $this->load->view('admin/suprimentoscategoria/edit.php');
    }

    function insertOrUpdate()
    {
        $posts = $this->input->post();

        $hasID = !empty($posts['id']); //used to define if it is an update or insert.

        /**** Formatando informacoes ****/
        $posts['slug'] = slug($posts['titulo']);
        $posts['ativo'] = (isset($posts['ativo'])&&$posts['ativo']=="on")?1:0;
        /****************  ****************/

        /**** Preparando campos para inserir no banco ****/
        $infoDB = array(
            'id'                => $posts['id'],
            'titulo'            => $posts['titulo'],
            'slug'              => $posts['slug'],
            'ativo'             => $posts['ativo']
        );
        /****************  ****************/

        /**** Insert no banco ****/
        $result = ($hasID) ? $this->SuprimentosCategoriamd->update($infoDB) : $this->SuprimentosCategoriamd->insert($infoDB);
        $id = ($hasID) ? $posts['id'] : $this->db->insert_id();
        gravaLog();
        /****************  ****************/

        $successText = 'Dados salvos!';
        $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText,base_url('index.php/suprimentoscategoriactrl'));
    }

    function status()
    {
        $posts = $this->input->post();
        $posts = array_filter($posts);

        if(isset($posts['ativo']))
        $posts['ativo'] = ($posts['ativo']=="on")?1:0;

        $result = $this->SuprimentosCategoriamd->update($posts);
        gravaLog();

        $successText = '';
        $failText = 'Erro ao salvar os dados, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText,base_url('index.php/suprimentoscategoriactrl'));
    }

    function delete()
    {
        $result = $this->SuprimentosCategoriamd->delete($this->input->post('id'));
        gravaLog();

        $successText = 'Registro Deletado!';
        $failText = 'Erro ao deeletar, tente novamente ou entre em contato com o administrador';
        $this->resultModel($result,$successText,$failText,base_url('index.php/suprimentoscategoriactrl'));
    }

    private function resultModel($result,$successText,$failText,$redirect){
        if(!$result) {
            // Erro ao salvar os dados
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'error');
            $this->session->set_flashdata('title', 'Erro');
            $this->session->set_flashdata('text', $failText);
            redirect($redirect);
        } else {
            $this->session->set_flashdata('modal', '1');
            $this->session->set_flashdata('icon', 'success');
            $this->session->set_flashdata('title', 'Sucesso');
            $this->session->set_flashdata('text', $successText);
            redirect($redirect);
        }
    }
}