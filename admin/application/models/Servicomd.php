<?php
Class Servicomd extends CI_Model
{
    function selectServico($busca = null)
    {
        $this->db->select('n.*', false);
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('servico n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectServicoById($id)
    {
        $query = $this->db->get_where('servico', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectServicoGaleriaById($id)
    {
        $query = $this->db->get_where('servicogaleria', array('id_servico' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insertServico($data){
        $this->db->insert('servico',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function updateServico($data)
    {
        $this->db->trans_start();
        $this->db->update('servico', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function deleteServico($id)
    {
        $this->db->delete('servico', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function insertServicoGaleria($data){
        $this->db->insert_batch('servicogaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteServicoGaleria($id)
    {
        $this->db->delete('servicogaleria', array('id_servico' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
?>
