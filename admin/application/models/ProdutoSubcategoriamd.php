<?php
Class ProdutoSubcategoriamd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria', false);
		$this->db->join('produtocategoria nc', 'nc.id = n.id_categoria');
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('produtosubcategoria n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectById($id)
    {
        $query = $this->db->get_where('produtoubcategoria', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('produtosubcategoriagaleria', array('id_produtoubcategoria' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('produtosubcategoria',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('produtosubcategoria', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('produtosubcategoria', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function selectByCategoria($categoria) 
    {
        $this->db->select('s.*, sc.titulo as categoria', false);
        $this->db->join('produtocategoria sc', 'sc.id = s.id_categoria');
        $query = $this->db->get('produtosubcategoria s');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }
}