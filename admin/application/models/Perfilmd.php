<?php
Class Perfilmd extends CI_Model
{
    function getLoginById($id)
    {
        $query = $this->db->get_where('login', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }

    function getEmail($email)
    {
        $query = $this->db->get_where('login', array('email' => $email));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }

    function updatePerfil($data)
    {
        $this->db->trans_start();
        $this->db->update('login', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }
}
?>
