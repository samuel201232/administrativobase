<?php
Class Suprimentosmd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria, ncs.titulo as subcategoria', false);
		$this->db->join('suprimentoscategoria nc', 'nc.id = n.id_categoria');
	    $this->db->join('suprimentossubcategoria ncs', 'ncs.id = n.id_subcategoria');
        $this->db->order_by("n.ordem", "asc");
        $query = $this->db->get('suprimentos n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectByCategoria($categoria) 
    {
        $this->db->select('s.*, sc.titulo as categoria', false);
        $this->db->join('suprimentoscategoria sc', 'sc.id = s.id_categoria');
        $query = $this->db->get('suprimentossubcategoria s');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }


    function selectById($id)
    {
        $query = $this->db->get_where('suprimentos', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('suprimentosgaleria', array('id_suprimento' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }


    //Aqui esta correto
    function insert($data){
        $this->db->insert('suprimentos',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('suprimentos', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('suprimentos', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function insertGaleria($data){
        $this->db->insert_batch('suprimentosgaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteGaleria($id)
    {
        $this->db->delete('suprimentosgaleria', array('id_suprimentos' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}