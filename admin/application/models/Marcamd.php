<?php
Class Marcamd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('pm.*', false);
        $this->db->order_by("pm.titulo", "asc");
        $query = $this->db->get('marcas pm');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectById($id)
    {
        $query = $this->db->get_where('marcas', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('marcagaleria', array('id_marca' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('marcas',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('marcas', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('marcas', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function insertGaleria($data){
        $this->db->insert_batch('marcagaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteGaleria($id)
    {
        $this->db->delete('marcagaleria', array('id_marca' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}