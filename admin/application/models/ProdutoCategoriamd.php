<?php
Class ProdutoCategoriamd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*', false);
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('produtocategoria n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectById($id)
    {
        $query = $this->db->get_where('produtocategoria', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('produtocategoriagaleria', array('id_produtocategoria' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('produtocategoria',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('produtocategoria', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('produtocategoria', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
?>
