<?php
Class SolucoesProdutomd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria, ncs.titulo as subcategoria', false);
		$this->db->join('solucoescategoria nc', 'nc.id = n.id_categoria');
		$this->db->join('solucoessubcategoria ncs', 'ncs.id = n.id_subcategoria');
		// $this->db->select('n.*', false);
        $this->db->order_by("n.ordem", "asc");
        $query = $this->db->get('solucoes n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectByCategoria($categoria) 
    {
        $this->db->select('s.*, sc.titulo as categoria', false);
        $this->db->join('solucoescategoria sc', 'sc.id = s.id_categoria');
        $query = $this->db->get('solucoessubcategoria s');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }


    function selectById($id)
    {
        $query = $this->db->get_where('solucoes', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('solucoesgaleria', array('id_solucao' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('solucoes',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('solucoes', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('solucoes', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function insertGaleria($data){
        $this->db->insert_batch('solucoesgaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteGaleria($id)
    {
        $this->db->delete('solucoesgaleria', array('id_solucao' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
