<?php
Class PecasSubcategoriamd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria', false);
        $this->db->join('pecascategorias nc', 'nc.id = n.id_categoria');
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('pecassubcategoria n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectById($id)
    {
        $query = $this->db->get_where('pecassubcategoria', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('pecassubcategoriagaleria', array('id_pecassubcategoria' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('pecassubcategoria',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('pecassubcategoria', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('pecassubcategoria', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}