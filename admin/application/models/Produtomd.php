<?php
Class Produtomd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria, ncs.titulo as subcategoria, pm.titulo as marca', false);
		$this->db->join('produtocategoria nc', 'nc.id = n.id_categoria');
		$this->db->join('produtosubcategoria ncs', 'ncs.id = n.id_subcategoria');
		$this->db->join('marcas pm', 'pm.id = n.id_marca');
        $this->db->order_by("n.ordem", "asc");
        $query = $this->db->get('produto n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }


    function selectById($id)
    {
        $query = $this->db->get_where('produto', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('produtogaleria', array('id_produto' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('produto',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('produto', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('produto', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

   function insertGaleria($data){
        $this->db->insert_batch('produtogaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteGaleria($id)
    {
        $this->db->delete('produtogaleria', array('id_produto' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}