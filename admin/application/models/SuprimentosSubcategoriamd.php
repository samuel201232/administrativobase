<?php
Class SuprimentosSubcategoriamd extends CI_Model
{
     function select($busca = null)
    {
        $this->db->select('n.*, nc.titulo as categoria', false);
        $this->db->join('suprimentoscategoria nc', 'nc.id = n.id_categoria');    
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);        }
        $query = $this->db->get('suprimentossubcategoria n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectByCategoria($categoria) 
    {
        $this->db->select('s.*, sc.titulo as categoria', false);
        $this->db->join('suprimentoscategoria sc', 'sc.id = s.id_categoria');
        $query = $this->db->get('suprimentosssubcategoria s');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }



    function selectById($id)
    {
        $query = $this->db->get_where('suprimentossubcategoria', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('suprimentossubcategoriagaleria', array('id_suprimentossubcategoria' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('suprimentossubcategoria',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('suprimentossubcategoria', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('suprimentossubcategoria', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}