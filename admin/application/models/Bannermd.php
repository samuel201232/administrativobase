<?php
Class Bannermd extends CI_Model
{
    function selectBanner()
    {
        $this->db->select('n.*', false);
        $query = $this->db->get('banner n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectBannerById($id)
    {
        $query = $this->db->get_where('banner', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function insertBanner($data){
        $this->db->insert('banner',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function updateBanner($data)
    {
        $this->db->trans_start();
        $this->db->update('banner', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function deleteBanner($id)
    {
        $this->db->delete('banner', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
?>
