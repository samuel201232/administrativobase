<?php
Class Noticiamd extends CI_Model
{
    function selectNoticia($busca = null)
    {
        $this->db->select('n.*,DATE_FORMAT(n.dia,"%d/%m/%Y") AS dia_formatado', false);
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('noticia n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectNoticiaById($id)
    {
        $query = $this->db->get_where('noticia', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectNoticiaGaleriaById($id)
    {
        $query = $this->db->get_where('noticiagaleria', array('id_noticia' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insertNoticia($data){
        $this->db->insert('noticia',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function updateNoticia($data)
    {
        $this->db->trans_start();
        $this->db->update('noticia', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function deleteNoticia($id)
    {
        $this->db->delete('noticia', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function insertNoticiaGaleria($data){
        $this->db->insert_batch('noticiagaleria', $data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function deleteNoticiaGaleria($id)
    {
        $this->db->delete('noticiagaleria', array('id_noticia' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}
?>
