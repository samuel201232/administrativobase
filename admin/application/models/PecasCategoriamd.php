<?php
Class PecasCategoriamd extends CI_Model
{
    function select($busca = null)
    {
        $this->db->select('n.*', false);
        if($busca){
            $this->db->like('n.titulo', $busca);
            $this->db->or_like('n.resumo', $busca);
            $this->db->or_like('n.descricao', $busca);
        }
        $query = $this->db->get('pecascategorias n');
        if($query->num_rows()) {
            return $query->result();
        }
        return false;
    }

    function selectById($id)
    {
        $query = $this->db->get_where('pecascategorias', array('id' => $id));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        return false;
    }

    function selectGaleriaById($id)
    {
        $query = $this->db->get_where('pecascategoriasgaleria', array('id_pecascategorias' => $id));
        if($query->num_rows() > 0) {
            return $query->result();
        }
        return false;
    }

    function insert($data){
        $this->db->insert('pecascategorias',$data);
        if ($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }

    function update($data)
    {
        $this->db->trans_start();
        $this->db->update('pecascategorias', $data, array('id' => $data['id']));
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }

    function delete($id)
    {
        $this->db->delete('pecascategorias', array('id' => $id));
        if($this->db->affected_rows() > 0) {
            return true;
        }
        return false;
    }
}