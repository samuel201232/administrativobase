<?php
Class Loginmd extends CI_Model
{
    function getLogin($email,$senha)
    {
        $query = $this->db->get_where('login', array('email' => $email, 'senha' => md5($senha)));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }

    function getEmail($email)
    {
        $query = $this->db->get_where('login', array('email' => $email));
        if($query->num_rows() == 1) {
            return $query->result();
        }
        else {
            return false;
        }
    }

    function updateSenha($email,$senha)
    {
        $this->db->trans_start();
        $this->db->set('senha', md5($senha));
        $this->db->where('email', $email);
        $this->db->update('login');
        $this->db->trans_complete();
        if ($this->db->trans_status() === TRUE) {
            return true;
        }
        return false;
    }
}
?>
